/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : laravel-vue-admin

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 28/05/2021 16:27:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cnpscy_admin_infos
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_infos`;
CREATE TABLE `cnpscy_admin_infos`  (
  `admin_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员Id',
  `login_num` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录次数',
  `created_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  INDEX `admin_id`(`admin_id`) USING BTREE,
  INDEX `create_time`(`created_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_admin_infos
-- ----------------------------
INSERT INTO `cnpscy_admin_infos` VALUES (1, 16, '', '', 1586429815, 1586429815);
INSERT INTO `cnpscy_admin_infos` VALUES (107, 0, '', '', 2020, 1600056174);
INSERT INTO `cnpscy_admin_infos` VALUES (108, 0, '', '', 2020, 2020);
INSERT INTO `cnpscy_admin_infos` VALUES (109, 0, '', '', 2020, 2020);
INSERT INTO `cnpscy_admin_infos` VALUES (110, 0, '', '', 2020, 2020);
INSERT INTO `cnpscy_admin_infos` VALUES (111, 0, '', '', 1597979069, 1597979069);
INSERT INTO `cnpscy_admin_infos` VALUES (112, 0, '0.0.0.0', '', 1597979413, 1597979413);
INSERT INTO `cnpscy_admin_infos` VALUES (113, 0, '0.0.0.0', '', 1597979452, 1597979452);
INSERT INTO `cnpscy_admin_infos` VALUES (114, 0, '0.0.0.0', '', 1597994535, 1597994535);
INSERT INTO `cnpscy_admin_infos` VALUES (117, 0, '0.0.0.0', '', 1600054407, 1600054407);
INSERT INTO `cnpscy_admin_infos` VALUES (118, 0, '0.0.0.0', '', 1600054522, 1600054522);
INSERT INTO `cnpscy_admin_infos` VALUES (119, 0, '0.0.0.0', '', 1600056188, 1600056188);
INSERT INTO `cnpscy_admin_infos` VALUES (123, 0, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36', 1608798983, 1608798983);

-- ----------------------------
-- Table structure for cnpscy_admin_login_logs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_login_logs`;
CREATE TABLE `cnpscy_admin_login_logs`  (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员Id',
  `created_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `log_status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '登录状态：1：成功；0：失败',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  `log_action` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求方法',
  `log_method` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求类型/请求方式',
  `request_data` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求参数',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：是；0：否',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `log_duration` decimal(20, 12) UNSIGNED NULL DEFAULT 0.000000000000,
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `admin_id`(`admin_id`) USING BTREE,
  INDEX `log_status`(`log_status`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员登录日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_admin_logs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_logs`;
CREATE TABLE `cnpscy_admin_logs`  (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员Id',
  `created_ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `log_status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态：1：成功；0：失败',
  `description` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  `log_action` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '请求方法',
  `log_method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '请求类型/请求方式',
  `request_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求参数',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：是；0：否',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `log_duration` decimal(20, 12) UNSIGNED NULL DEFAULT 0.000000000000,
  `request_url` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `admin_id`(`admin_id`) USING BTREE,
  INDEX `log_status`(`log_status`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员操作日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_admin_menus
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_menus`;
CREATE TABLE `cnpscy_admin_menus`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '菜单栏目表',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级id',
  `menu_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '栏目名称',
  `vue_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `vue_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'VUE路由路径',
  `vue_redirect` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT 'Vue的redirect',
  `vue_icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图标',
  `vue_component` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'VUE文件路径',
  `vue_meta` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `external_links` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '外链',
  `api_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '接口路由',
  `api_method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '接口的请求方式',
  `menu_sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `is_hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否隐藏菜单栏：1：是；0：否',
  `is_check` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否可用：1：可用；0：禁用',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`menu_id`) USING BTREE,
  INDEX `admin_menus_parent_id_index`(`parent_id`) USING BTREE,
  INDEX `admin_menus_is_check_index`(`is_check`) USING BTREE,
  INDEX `admin_menus_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 79 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_admin_menus
-- ----------------------------
INSERT INTO `cnpscy_admin_menus` VALUES (1, 0, '首页', 'Dashboard', '/', '/dashboard', 'fa fa-dashboard', 'Layout', '', '', '', '', 1, 0, 1, 0, 1601434917, 1621561441);
INSERT INTO `cnpscy_admin_menus` VALUES (2, 0, '权限管理', 'permissionManage', '/rabc', '/rabc/menus', 'el-icon-lock', 'Layout', '', '', '', '', 2, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (3, 0, '系统设置', 'systemSettings', '', '/configs', 'el-icon-setting', 'Layout', '', '', '', '', 3, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (4, 0, '文章管理', 'articleManage', '/articles', '', 'el-icon-tickets', 'Layout', '', '', '', '', 4, 0, 1, 0, 1609127616, 1621560264);
INSERT INTO `cnpscy_admin_menus` VALUES (5, 1, 'Dashboard', 'Dashboard', 'dashboard', '', '', 'dashboard/index', '', '', 'index', 'GET', 1, 0, 1, 1, 1601434917, 1601437498);
INSERT INTO `cnpscy_admin_menus` VALUES (6, 2, '菜单管理', 'menuManage', 'menus', '', '', 'admin_menus/index', '', '', 'admin/admin_menus', 'GET', 1, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (7, 2, '角色管理', 'roleManage', 'roles', '', '', 'admin_roles/index', '', '', 'admin/admin_roles', 'GET', 2, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (8, 2, '管理员管理', 'adminManage', 'admins', '', '', 'admins/index', '', '', 'admin/admins', 'GET', 3, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (9, 3, '配置管理', 'configManage', '/configs', '', '', 'configs/index', '', '', 'admin/configs', 'GET', 1, 0, 1, 0, 1609126826, 1609126826);
INSERT INTO `cnpscy_admin_menus` VALUES (10, 3, '友情链接', 'friendlinks', '/friendlinks', '', '', 'friendlinks/index', '', '', 'admin/friendlinks', 'GET', 2, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (11, 3, 'Banner管理', 'bannerManage', '/banners', '', '', 'banners/index', '', '', 'admin/banners', 'GET', 3, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (12, 4, '文章分类', 'articleCategory', 'categories', '', '', 'article_categories/index', '', '', 'admin/article_categories', 'GET', 1, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (13, 4, '文章列表', 'articleLists', '', '', '', 'articles/index', '', '', 'admin/articles', 'GET', 2, 0, 1, 0, 1609126826, 1609317417);
INSERT INTO `cnpscy_admin_menus` VALUES (14, 6, '下拉列表', '', '', '', '', '', '', '', 'admin/admin_menus/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (15, 6, '新增', '', '', '', '', '', '', '', 'admin/admin_menus/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (16, 6, '更新', '', '', '', '', '', '', '', 'admin/admin_menus/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (17, 6, '删除', '', '', '', '', '', '', '', 'admin/admin_menus/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (18, 6, '详情', '', '', '', '', '', '', '', 'admin/admin_menus/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (19, 7, '新增', '', '', '', '', '', '', '', 'admin/admin_roles/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (20, 7, '详情', '', '', '', '', '', '', '', 'admin/admin_roles/detail', 'GET', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (21, 7, '更新', '', '', '', '', '', '', '', 'admin/admin_roles/update', 'PUT', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (22, 7, '删除', '', '', '', '', '', '', '', 'admin/admin_roles/delete', 'DELETE', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (23, 7, '下拉列表', '', '', '', '', '', '', '', 'admin/admin_roles/getSelectLists', 'GET', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (24, 8, '新增', '', '', '', '', '', '', '', 'admin/admins/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (25, 8, '详情', '', '', '', '', '', '', '', 'admin/admins/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (26, 8, '更新', '', '', '', '', '', '', '', 'admin/admins/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (27, 8, '删除', '', '', '', '', '', '', '', 'admin/admins/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (28, 8, '下拉列表', '', '', '', '', '', '', '', 'admin/admins/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (29, 9, '新增', '', '', '', '', '', '', '', 'admin/configs/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (30, 3, '配置详情（禁止修改父级，VUE的路由层级嵌套问题）', '详情', '/configs/detail', '', '', 'configs/detail', '', '', 'admin/configs/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (31, 9, '更新', '', '', '', '', '', '', '', 'admin/configs/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (32, 9, '删除', '', '', '', '', '', '', '', 'admin/configs/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (33, 9, '下拉列表', '', '', '', '', '', '', '', 'admin/configs/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (34, 10, '新增', '', '', '', '', '', '', '', 'admin/friendlinks/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (35, 10, '详情', '', '', '', '', '', '', '', 'admin/friendlinks/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (36, 10, '更新', '', '', '', '', '', '', '', 'admin/friendlinks/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (37, 10, '删除', '', '', '', '', '', '', '', 'admin/friendlinks/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (38, 10, '下拉列表', '', '', '', '', '', '', '', 'admin/friendlinks/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (39, 11, '新增', '', '', '', '', '', '', '', 'admin/banners/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (40, 11, '详情', '', '', '', '', '', '', '', 'admin/banners/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (41, 11, '更新', '', '', '', '', '', '', '', 'admin/banners/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (42, 11, '删除', '', '', '', '', '', '', '', 'admin/banners/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (43, 11, '下拉列表', '', '', '', '', '', '', '', 'admin/banners/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (44, 12, '新增', '', '', '', '', '', '', '', 'admin/article_categories/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (45, 12, '详情', '', '', '', '', '', '', '', 'admin/article_categories/detail', 'GET', 2, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (46, 12, '更新', '', '', '', '', '', '', '', 'admin/article_categories/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (47, 12, '删除', '', '', '', '', '', '', '', 'admin/article_categories/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (48, 12, '下拉列表', '', '', '', '', '', '', '', 'admin/article_categories/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (49, 13, '新增', '', '', '', '', '', '', '', 'admin/articles/create', 'POST', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (50, 4, '文章详情（禁止修改父级，VUE的路由层级嵌套问题）', '', 'detail', '', '', 'articles/detail', '', '', 'admin/articles/detail', 'GET', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (51, 13, '更新', '', '', '', '', '', '', '', 'admin/articles/update', 'PUT', 3, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (52, 13, '删除', '', '', '', '', '', '', '', 'admin/articles/delete', 'DELETE', 4, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (53, 13, '下拉列表', '', '', '', '', '', '', '', 'admin/articles/getSelectLists', 'GET', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (54, 0, '日志管理', 'logManage', '/log', 'adminloginlogs', 'el-icon-s-order', 'Layout', '', '', '', '', 5, 0, 1, 0, 1609126942, 1609126965);
INSERT INTO `cnpscy_admin_menus` VALUES (55, 54, '管理员登录日志', 'adminLoginLog', 'adminloginlogs', '', '', 'adminloginlogs/index', '', '', 'admin/adminloginlogs', 'GET', 1, 0, 1, 0, 1609127601, 1609127601);
INSERT INTO `cnpscy_admin_menus` VALUES (56, 54, '管理员操作日志', 'adminOperationLog', 'adminlogs', '', '', 'adminlogs/index', '', '', 'admin/adminlogs', 'GET', 2, 0, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (57, 7, '状态变更', '', '', '', '', '', '', '', 'admin/admin_roles/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (58, 6, '状态变更', '', '', '', '', '', '', '', 'admin/admin_menus/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (59, 8, '状态变更', '', '', '', '', '', '', '', 'admin/admins/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (60, 55, '删除', '', '', '', '', '', '', '', 'admin/adminloginlogs/delete', 'DELETE', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (61, 56, '删除', '', '', '', '', '', '', '', 'admin/adminlogs/delete', 'DELETE', 1, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (62, 12, '状态变更', '', '', '', '', '', '', '', 'admin/article_categories/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (63, 11, '状态变更', '', '', '', '', '', '', '', 'admin/banners/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (64, 10, '状态变更', '', '', '', '', '', '', '', 'admin/friendlinks/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (65, 9, '状态变更', '', '', '', '', '', '', '', 'admin/configs/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (66, 13, '状态变更', '', '', '', '', '', '', '', 'admin/articles/changeFiledStatus', 'PUT', 5, 1, 1, 0, 1609127616, 1609127616);
INSERT INTO `cnpscy_admin_menus` VALUES (67, 3, '版本管理', 'versionManage', 'versions', '', '', 'versions/index', '', '', 'admin/versions', 'GET', 4, 0, 1, 0, 1610001528, 1610001555);
INSERT INTO `cnpscy_admin_menus` VALUES (68, 67, '新增', '', '', '', '', '', '', '', 'admin/versions/create', 'POST', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` VALUES (69, 67, '详情', '', '', '', '', '', '', '', 'admin/versions/detail', 'GET', 2, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` VALUES (70, 67, '更新', '', '', '', '', '', '', '', 'admin/versions/update', 'PUT', 3, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` VALUES (71, 67, '删除', '', '', '', '', '', '', '', 'admin/versions/delete', 'DELETE', 4, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` VALUES (72, 4, '文章标签', 'articleLabels', 'labels', '', '', 'article_labels/index', '', '', 'admin/article_labels', 'GET', 3, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` VALUES (73, 72, '新增', '', '', '', '', '', '', '', 'admin/article_labels/create', 'POST', 0, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` VALUES (74, 72, '详情', '', '', '', '', '', '', '', 'admin/article_labels/detail', 'GET', 0, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` VALUES (75, 72, '更新', '', '', '', '', '', '', '', 'admin/article_labels/update', 'PUT', 0, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` VALUES (76, 72, '删除', '', '', '', '', '', '', '', 'admin/article_labels/delete', 'DELETE', 0, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` VALUES (77, 3, '数据库管理', 'databaseManage', 'databases', '', '', 'databases/index', '', '', 'databases', 'GET', 5, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus` VALUES (78, 3, '备份管理', 'backupsManage', 'backups', '', '', 'databases/backups', '', '', 'databases/backups', 'GET', 6, 0, 1, 0, 0, 0);

-- ----------------------------
-- Table structure for cnpscy_admin_menus_copy1
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_menus_copy1`;
CREATE TABLE `cnpscy_admin_menus_copy1`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '菜单栏目表',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级id',
  `menu_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '栏目名称',
  `vue_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `vue_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'VUE路由路径',
  `vue_redirect` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT 'Vue的redirect',
  `vue_icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图标',
  `vue_component` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'VUE文件路径',
  `vue_meta` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `external_links` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '外链',
  `api_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '接口路由',
  `api_method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '接口的请求方式',
  `menu_sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `is_hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否隐藏菜单栏：1：是；0：否',
  `is_check` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否可用：1：可用；0：禁用',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`menu_id`) USING BTREE,
  INDEX `admin_menus_parent_id_index`(`parent_id`) USING BTREE,
  INDEX `admin_menus_is_check_index`(`is_check`) USING BTREE,
  INDEX `admin_menus_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_admin_menus_copy1
-- ----------------------------
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (1, 0, '首页', 'Dashboard', '/', '/dashboard', 'el-icon-table', 'Layout', '', '', '', '', 1, 0, 1, 0, 1601434917, 1601437649);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (2, 0, '权限管理', 'Permission', '/rabc', '/rabc/menus', 'el-icon-lock', 'Layout', '', '', '', '', 2, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (3, 0, '系统设置', 'setting', '/setting', '', 'el-icon-setting', 'Layout', '', '', '', '', 3, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (4, 0, '文章管理', 'articles', '/articles', '/articles/index', 'el-icon-notebook-2', 'Layout', '', '', '', '', 4, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (5, 1, 'Dashboard', 'Dashboard', 'dashboard', '', '', 'dashboard/index', '', '', 'index', 'GET', 1, 0, 1, 1, 1601434917, 1601437498);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (6, 2, '菜单管理', 'MenuPermission', 'menus', '', '', 'admin_menus/index', '', '', 'admin/admin_menus', 'GET', 1, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (7, 2, '角色管理', 'RolePermission', 'roles', '', '', 'admin_roles/index', '', '', 'admin/admin_roles', 'GET', 2, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (8, 2, '管理员管理', 'AdminPermission', 'admins', '', '', 'admins/index', '', '', 'admin/admins', 'GET', 3, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (9, 3, '配置管理', 'configs', 'configs', '', '', 'setting/configs/index', '', '', 'admin/configs', 'GET', 1, 0, 1, 0, 0, 1609126826);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (10, 3, '友情链接', 'friendlinks', 'friendlinks', '', '', 'setting/friendlinks/index', '', '', 'admin/friendlinks', 'GET', 2, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (11, 3, 'Banner管理', 'banners', 'banners', '', '', 'setting/banners/index', '', '', 'admin/banners', 'GET', 3, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (12, 4, '文章分类', '', 'categorys', '', '', 'articles/category', '', '', 'admin/articlecategories', 'GET', 1, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (13, 4, '文章列表', '', 'index', '', '', 'articles/index', '', '', 'admin/articles', 'GET', 2, 0, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (14, 6, '下拉列表', '', '', '', '', '', '', '', 'admin/admin_menus/getSelectLists', 'GET', 5, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (15, 6, '新增', '', '', '', '', '', '', '', 'admin/admin_menus/create', 'POST', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (16, 6, '更新', '', '', '', '', '', '', '', 'admin/admin_menus/update', 'PUT', 3, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (17, 6, '删除', '', '', '', '', '', '', '', 'admin/admin_menus/delete', 'DELETE', 4, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (18, 6, '详情', '', '', '', '', '', '', '', 'admin/admin_menus/detail', 'GET', 2, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (19, 7, '新增', '', '', '', '', '', '', '', 'admin/admin_roles/create', 'POST', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (20, 7, '详情', '', '', '', '', '', '', '', 'admin/admin_roles/detail', 'PUT', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (21, 7, '更新', '', '', '', '', '', '', '', 'admin/admin_roles/update', 'DELETE', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (22, 7, '删除', '', '', '', '', '', '', '', 'admin/admin_roles/delete', 'GET', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (23, 7, '下拉列表', '', '', '', '', '', '', '', 'admin/admin_roles/getSelectLists', 'GET', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (24, 8, '新增', '', '', '', '', '', '', '', 'admin/admins/create', 'POST', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (25, 8, '详情', '', '', '', '', '', '', '', 'admin/admins/detail', 'PUT', 2, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (26, 8, '更新', '', '', '', '', '', '', '', 'admin/admins/update', 'DELETE', 3, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (27, 8, '删除', '', '', '', '', '', '', '', 'admin/admins/delete', 'GET', 4, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (28, 8, '下拉列表', '', '', '', '', '', '', '', 'admin/admins/getSelectLists', 'GET', 5, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (29, 9, '新增', '', '', '', '', '', '', '', 'admin/configs/create', 'POST', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (30, 9, '详情', '', '', '', '', '', '', '', 'admin/configs/detail', 'PUT', 2, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (31, 9, '更新', '', '', '', '', '', '', '', 'admin/configs/update', 'DELETE', 3, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (32, 9, '删除', '', '', '', '', '', '', '', 'admin/configs/delete', 'GET', 4, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (33, 9, '下拉列表', '', '', '', '', '', '', '', 'admin/configs/getSelectLists', 'GET', 5, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (34, 10, '新增', '', '', '', '', '', '', '', 'admin/friendlinks/create', 'POST', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (35, 10, '详情', '', '', '', '', '', '', '', 'admin/friendlinks/detail', 'PUT', 2, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (36, 10, '更新', '', '', '', '', '', '', '', 'admin/friendlinks/update', 'DELETE', 3, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (37, 10, '删除', '', '', '', '', '', '', '', 'admin/friendlinks/delete', 'GET', 4, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (38, 10, '下拉列表', '', '', '', '', '', '', '', 'admin/friendlinks/getSelectLists', 'GET', 5, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (39, 11, '新增', '', '', '', '', '', '', '', 'admin/banners/create', 'POST', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (40, 11, '详情', '', '', '', '', '', '', '', 'admin/banners/detail', 'PUT', 2, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (41, 11, '更新', '', '', '', '', '', '', '', 'admin/banners/update', 'DELETE', 3, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (42, 11, '删除', '', '', '', '', '', '', '', 'admin/banners/delete', 'GET', 4, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (43, 11, '下拉列表', '', '', '', '', '', '', '', 'admin/banners/getSelectLists', 'GET', 5, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (44, 12, '新增', '', '', '', '', '', '', '', 'admin/articlecategories/create', 'POST', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (45, 12, '详情', '', '', '', '', '', '', '', 'admin/articlecategories/detail', 'PUT', 2, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (46, 12, '更新', '', '', '', '', '', '', '', 'admin/articlecategories/update', 'DELETE', 3, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (47, 12, '删除', '', '', '', '', '', '', '', 'admin/articlecategories/delete', 'GET', 4, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (48, 12, '下拉列表', '', '', '', '', '', '', '', 'admin/articlecategories/getSelectLists', 'GET', 5, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (49, 13, '新增', '', '', '', '', '', '', '', 'admin/articles/create', 'POST', 1, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (50, 13, '详情', '', '', '', '', '', '', '', 'admin/articles/detail', 'PUT', 2, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (51, 13, '更新', '', '', '', '', '', '', '', 'admin/articles/update', 'DELETE', 3, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (52, 13, '删除', '', '', '', '', '', '', '', 'admin/articles/delete', 'GET', 4, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (53, 13, '下拉列表', '', '', '', '', '', '', '', 'admin/articles/getSelectLists', 'GET', 5, 1, 1, 0, 0, 0);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (54, 0, '日志管理', '', '/log', 'adminloginlogs', 'el-icon-s-order', 'Layout', '', '', '', '', 5, 0, 1, 0, 1609126942, 1609126965);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (55, 54, '管理员登录日志', '', 'adminloginlogs', '', '', 'adminloginlogs/index', '', '', '', '', 1, 0, 1, 0, 1609127601, 1609127601);
INSERT INTO `cnpscy_admin_menus_copy1` VALUES (56, 54, '管理员日志', '', 'adminlogs', '', '', 'adminlogs/index', '', '', '', '', 2, 0, 1, 0, 1609127616, 1609127616);

-- ----------------------------
-- Table structure for cnpscy_admin_menus_old
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_menus_old`;
CREATE TABLE `cnpscy_admin_menus_old`  (
  `menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '菜单栏目表',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级id',
  `menu_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '栏目名称',
  `vue_path` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'javascript:;' COMMENT '控制器/方法',
  `api_url` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '接口地址',
  `menu_icon` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图标',
  `menu_level` smallint(5) UNSIGNED NOT NULL DEFAULT 1 COMMENT '栏目层级',
  `check_auth` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否检测权限【0.不检测；1.检测】',
  `menu_sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `is_hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否展示左侧：1：展示；0：隐藏',
  `is_check` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否可用：1：可用；0：禁用',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：未',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `vue_component` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'javascript:;' COMMENT '控制器/方法',
  `vue_meta` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `external_links` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`menu_id`) USING BTREE,
  INDEX `admin_menus_created_time_index`(`created_time`) USING BTREE,
  INDEX `admin_menus_menu_sort_index`(`menu_sort`) USING BTREE,
  INDEX `admin_menus_is_left_index`(`is_hidden`) USING BTREE,
  INDEX `admin_menus_parent_id_index`(`parent_id`) USING BTREE,
  INDEX `admin_menus_check_auth_index`(`check_auth`) USING BTREE,
  INDEX `admin_menus_menu_level_index`(`menu_level`) USING BTREE,
  INDEX `admin_menus_is_check_index`(`is_check`) USING BTREE,
  INDEX `admin_menus_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 87 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_admin_menus_old
-- ----------------------------
INSERT INTO `cnpscy_admin_menus_old` VALUES (1, 0, '首页', 'indexs/main', '', 'fa fa-home', 1, 1, 1, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (2, 0, '权限管理', 'javascript:;', '', 'fa fa-sun-o', 1, 1, 2, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (3, 0, '系统设置', 'javascript:;', '', 'fa fa-cogs', 1, 1, 3, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (4, 0, '博客管理', 'javascript:;', '', 'fa fa-file', 1, 1, 4, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (5, 0, '日志管理', 'javascript:;', '', 'fa fa-sticky-note-o', 1, 1, 5, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (6, 2, '菜单管理', 'adminmenus/index', '', '', 1, 1, 3, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (7, 2, '管理员管理', 'admins/index', '', '', 1, 1, 1, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (8, 2, '角色管理', 'roles/index', '', '', 1, 1, 2, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (9, 3, '网站设置', 'websites/index', '', '', 1, 1, 1, 1, 1, 0, 1542438844, 1542612306, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (24, 4, '栏目列表', 'blogmenus/index', '', '', 1, 1, 1, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (25, 4, '文章标签', 'articleLabels/index', '', '', 1, 1, 2, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (26, 4, '文章列表', 'articles/index', '', '', 1, 1, 3, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (28, 5, '登录日志', 'adminloginlogs/index', '', '', 1, 1, 2, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (30, 7, '详情', 'admins/detail', '', '', 1, 1, 1, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (31, 7, '新增', 'admins/create', '', '', 1, 1, 2, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (32, 7, '编辑', 'admins/update', '', '', 1, 1, 3, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (33, 7, '删除', 'admins/delete', '', '', 1, 1, 4, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (34, 7, '状态变动', 'admins/changeStatus', '', '', 1, 1, 5, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (35, 8, '详情', 'roles/detail', '', '', 1, 1, 1, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (36, 8, '新增', 'roles/create', '', '', 1, 1, 2, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (37, 8, '编辑', 'roles/update', '', '', 1, 1, 3, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (38, 8, '删除', 'roles/delete', '', '', 1, 1, 4, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (39, 8, '状态变动', 'roles/changeStatus', '', '', 1, 1, 5, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (40, 6, '详情', 'menus/detail', '', '', 1, 1, 1, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (41, 6, '新增', 'menus/create', '', '', 1, 1, 2, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (42, 6, '编辑', 'menus/update', '', '', 1, 1, 3, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (43, 6, '删除', 'menus/delete', '', '', 1, 1, 4, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (44, 6, '状态变动', 'menus/changeStatus', '', '', 1, 1, 5, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (45, 24, '详情', 'blogmenus/detail', '', '', 1, 1, 1, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (46, 24, '新增', 'blogmenus/create', '', '', 1, 1, 2, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (47, 24, '编辑', 'blogmenus/update', '', '', 1, 1, 3, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (48, 24, '删除', 'admins/create', '', '', 1, 1, 4, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (49, 24, '状态变动', 'admins/update', '', '', 1, 1, 5, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (50, 25, '详情', 'admins/delete', '', '', 1, 1, 1, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (51, 25, '新增', 'admins/changeStatus', '', '', 1, 1, 2, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (52, 25, '编辑', 'roles/detail', '', '', 1, 1, 3, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (53, 25, '删除', 'roles/create', '', '', 1, 1, 4, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (54, 26, '详情', 'roles/update', '', '', 1, 1, 1, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (55, 26, '新增', 'roles/delete', '', '', 1, 1, 2, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (56, 26, '编辑', 'roles/changeStatus', '', '', 1, 1, 3, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (57, 26, '删除', 'menus/detail', '', '', 1, 1, 4, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (58, 26, '状态变动', 'menus/create', '', '', 1, 1, 5, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (59, 6, '左侧栏目权限', 'menus/update', '', '', 1, 1, 6, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (63, 4, 'Banner管理', 'banners/index', '', '', 1, 1, 4, 1, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (64, 63, '详情', 'admins/create', '', '', 1, 1, 1, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (65, 63, '新增', 'admins/update', '', '', 1, 1, 2, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (66, 63, '编辑', 'admins/delete', '', '', 1, 1, 3, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (67, 63, '删除', 'admins/changeStatus', '', '', 1, 1, 4, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (68, 63, '状态变动', 'roles/detail', '', '', 1, 1, 5, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (70, 28, '详情', 'roles/update', '', '', 1, 1, 1, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (72, 28, '删除', 'roles/changeStatus', '', '', 1, 1, 2, 0, 1, 0, 1542438844, 1542438844, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (74, 3, '配置管理', 'configs/index', '', '', 1, 1, 4, 1, 1, 0, 1542599987, 1542600147, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (75, 74, '详情', 'admins/create', '', '', 1, 1, 1, 0, 1, 0, 1542609016, 1542609016, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (76, 74, '新增', 'admins/update', '', '', 1, 1, 2, 0, 1, 0, 1542609024, 1542609024, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (77, 74, '编辑', 'admins/delete', '', '', 1, 1, 3, 0, 1, 0, 1542609039, 1542609039, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (78, 74, '删除', 'admins/changeStatus', '', '', 1, 1, 4, 0, 1, 0, 1542609050, 1542609050, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (79, 74, '状态变动', 'roles/detail', '', '', 1, 1, 5, 0, 1, 0, 1542609065, 1542609065, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (80, 74, '同步配置', 'roles/create', '', '', 1, 1, 6, 0, 1, 0, 1542615988, 1542615988, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (81, 3, '友情链接', 'friendlinks/index', '', '', 1, 1, 5, 1, 1, 0, 1542620811, 1542620811, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (82, 81, '详情', 'friendlinks/detail', '', '', 1, 1, 1, 0, 1, 0, 1542620827, 1542620827, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (83, 81, '新增', 'friendlinks/create', '', '', 1, 1, 2, 0, 1, 0, 1542620838, 1542620838, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (84, 81, '编辑', 'friendlinks/update', '', '', 1, 1, 3, 0, 1, 0, 1542620849, 1542620849, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (85, 81, '删除', 'friendlinks/delete', '', '', 1, 1, 4, 0, 1, 0, 1542620859, 1542620859, 'javascript:;', '', '');
INSERT INTO `cnpscy_admin_menus_old` VALUES (86, 81, '状态变动', 'friendlinks/changeStatus', '', '', 1, 1, 5, 0, 1, 0, 1542620882, 1542620882, 'javascript:;', '', '');

-- ----------------------------
-- Table structure for cnpscy_admin_operation_logs_2020_04
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_operation_logs_2020_04`;
CREATE TABLE `cnpscy_admin_operation_logs_2020_04`  (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员Id',
  `created_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  `log_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态：1：成功；0：失败',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  `log_action` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求方法',
  `log_method` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求类型/请求方式',
  `request_data` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请求参数',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：是；0：否',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `admin_id`(`admin_id`) USING BTREE,
  INDEX `log_status`(`log_status`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员操作日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_admin_role_with_menus
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_role_with_menus`;
CREATE TABLE `cnpscy_admin_role_with_menus`  (
  `with_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '角色Id',
  `menu_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '菜单Id',
  PRIMARY KEY (`with_id`) USING BTREE,
  INDEX `role_with_menus_role_id_index`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 217 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_admin_role_with_menus
-- ----------------------------
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (1, 1, 1);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (2, 1, 2);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (3, 1, 3);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (4, 1, 4);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (5, 1, 5);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (6, 1, 6);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (7, 1, 7);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (8, 1, 8);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (9, 1, 9);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (10, 1, 10);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (11, 1, 11);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (12, 1, 12);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (13, 1, 13);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (42, 1, 14);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (43, 1, 15);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (44, 1, 16);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (45, 1, 17);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (46, 1, 18);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (47, 1, 19);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (48, 1, 20);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (49, 1, 21);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (50, 1, 22);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (51, 1, 23);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (52, 1, 24);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (53, 1, 25);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (54, 1, 26);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (55, 1, 27);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (56, 1, 28);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (57, 1, 29);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (58, 1, 30);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (59, 1, 31);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (60, 1, 32);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (61, 1, 33);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (62, 1, 34);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (63, 1, 35);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (64, 1, 36);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (65, 1, 37);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (66, 1, 38);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (67, 1, 39);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (68, 1, 40);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (69, 1, 41);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (70, 1, 42);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (71, 1, 43);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (72, 1, 44);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (73, 1, 45);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (74, 1, 46);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (75, 1, 47);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (76, 1, 48);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (77, 1, 49);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (78, 1, 50);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (79, 1, 51);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (80, 1, 52);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (81, 1, 53);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (99, 2, 1);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (100, 2, 2);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (101, 2, 3);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (102, 2, 4);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (114, 1, 57);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (115, 1, 58);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (116, 1, 59);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (117, 1, 60);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (118, 1, 61);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (119, 1, 62);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (120, 1, 63);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (121, 1, 64);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (122, 1, 65);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (123, 1, 66);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (124, 1, 55);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (125, 1, 56);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (126, 12, 1);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (127, 12, 2);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (128, 12, 3);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (129, 12, 4);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (130, 12, 54);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (131, 15, 1);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (132, 15, 2);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (133, 14, 1);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (134, 15, 6);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (135, 14, 2);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (136, 15, 7);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (137, 14, 6);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (138, 15, 14);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (139, 14, 7);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (140, 15, 15);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (141, 14, 14);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (142, 15, 16);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (143, 14, 15);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (144, 15, 17);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (145, 14, 16);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (146, 15, 18);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (147, 14, 17);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (148, 15, 19);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (149, 14, 18);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (150, 15, 58);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (151, 14, 19);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (152, 14, 58);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (153, 15, 3);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (154, 15, 4);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (155, 15, 8);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (156, 15, 9);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (157, 15, 10);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (158, 15, 11);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (159, 15, 12);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (160, 15, 13);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (161, 15, 20);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (162, 15, 21);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (163, 15, 22);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (164, 15, 23);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (165, 15, 24);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (166, 15, 25);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (167, 15, 26);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (168, 15, 27);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (169, 15, 28);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (170, 15, 29);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (171, 15, 30);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (172, 15, 31);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (173, 15, 32);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (174, 15, 33);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (175, 15, 34);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (176, 15, 35);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (177, 15, 36);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (178, 15, 37);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (179, 15, 38);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (180, 15, 39);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (181, 15, 40);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (182, 15, 41);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (183, 15, 42);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (184, 15, 43);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (185, 15, 44);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (186, 15, 45);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (187, 15, 46);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (188, 15, 47);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (189, 15, 48);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (190, 15, 49);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (191, 15, 50);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (192, 15, 51);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (193, 15, 52);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (194, 15, 53);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (195, 15, 54);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (196, 15, 55);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (197, 15, 56);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (198, 15, 57);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (199, 15, 59);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (200, 15, 60);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (201, 15, 61);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (202, 15, 62);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (203, 15, 63);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (204, 15, 64);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (205, 15, 65);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (206, 15, 66);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (207, 1, 67);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (208, 1, 68);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (209, 1, 69);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (210, 1, 70);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (211, 1, 71);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (212, 1, 72);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (213, 1, 73);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (214, 1, 74);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (215, 1, 75);
INSERT INTO `cnpscy_admin_role_with_menus` VALUES (216, 1, 76);

-- ----------------------------
-- Table structure for cnpscy_admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_roles`;
CREATE TABLE `cnpscy_admin_roles`  (
  `role_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色表',
  `role_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_remarks` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `is_check` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否可用：1：可用；0：禁用',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`role_id`) USING BTREE,
  INDEX `blog_admin_roles_created_time_index`(`created_time`) USING BTREE,
  INDEX `blog_admin_roles_is_delete_index`(`is_delete`) USING BTREE,
  INDEX `blog_admin_roles_is_check_index`(`is_check`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_admin_roles
-- ----------------------------
INSERT INTO `cnpscy_admin_roles` VALUES (1, '超级管理员', '权限最大的人', 1, 0, 1577671704, 1601293601);
INSERT INTO `cnpscy_admin_roles` VALUES (2, 'test', '备注信息', 0, 0, 1598348975, 1609318749);
INSERT INTO `cnpscy_admin_roles` VALUES (3, '111', '2121', 0, 1, 1608795030, 1609827748);
INSERT INTO `cnpscy_admin_roles` VALUES (7, '31232', '12321', 0, 0, 1608795106, 1609901582);
INSERT INTO `cnpscy_admin_roles` VALUES (10, '42342432', '42423', 1, 0, 1608795181, 1609901579);
INSERT INTO `cnpscy_admin_roles` VALUES (11, '3123213', '2132321', 0, 1, 1608796971, 1609817914);
INSERT INTO `cnpscy_admin_roles` VALUES (12, '111', '111', 0, 1, 1609828403, 1609830841);
INSERT INTO `cnpscy_admin_roles` VALUES (13, '111', '111', 1, 0, 1609831035, 1609831035);
INSERT INTO `cnpscy_admin_roles` VALUES (14, '全部权限的角色', '', 1, 1, 1609832355, 1609832468);
INSERT INTO `cnpscy_admin_roles` VALUES (15, '全部权限的角色', '', 1, 0, 1609832355, 1609832355);

-- ----------------------------
-- Table structure for cnpscy_admin_with_roles
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admin_with_roles`;
CREATE TABLE `cnpscy_admin_with_roles`  (
  `with_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '角色Id',
  `admin_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员Id',
  PRIMARY KEY (`with_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `admin_id`(`admin_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台-角色与管理员关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_admin_with_roles
-- ----------------------------
INSERT INTO `cnpscy_admin_with_roles` VALUES (1, 1, 2);
INSERT INTO `cnpscy_admin_with_roles` VALUES (3, 2, 102);
INSERT INTO `cnpscy_admin_with_roles` VALUES (6, 1, 107);
INSERT INTO `cnpscy_admin_with_roles` VALUES (9, 1, 119);
INSERT INTO `cnpscy_admin_with_roles` VALUES (11, 3, 119);
INSERT INTO `cnpscy_admin_with_roles` VALUES (12, 11, 119);
INSERT INTO `cnpscy_admin_with_roles` VALUES (14, 1, 123);
INSERT INTO `cnpscy_admin_with_roles` VALUES (15, 10, 123);
INSERT INTO `cnpscy_admin_with_roles` VALUES (16, 11, 123);
INSERT INTO `cnpscy_admin_with_roles` VALUES (17, 1, 1);
INSERT INTO `cnpscy_admin_with_roles` VALUES (18, 11, 1);
INSERT INTO `cnpscy_admin_with_roles` VALUES (20, 3, 123);
INSERT INTO `cnpscy_admin_with_roles` VALUES (21, 7, 123);
INSERT INTO `cnpscy_admin_with_roles` VALUES (22, 2, 123);
INSERT INTO `cnpscy_admin_with_roles` VALUES (23, 2, 118);
INSERT INTO `cnpscy_admin_with_roles` VALUES (24, 7, 118);
INSERT INTO `cnpscy_admin_with_roles` VALUES (25, 10, 118);

-- ----------------------------
-- Table structure for cnpscy_admins
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_admins`;
CREATE TABLE `cnpscy_admins`  (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm' COMMENT '密码',
  `admin_head` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像',
  `admin_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱',
  `login_token` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'login_token',
  `is_check` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '登陆状态[0.尚未开放；1.正常；2.禁用]',
  `kick_out` tinyint(1) UNSIGNED NOT NULL DEFAULT 2 COMMENT '是否踢出登录[0：表示在线；1：踢出登录；2.未登录]',
  `use_role` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '正在使用的角色Id',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：是；0：否',
  PRIMARY KEY (`admin_id`) USING BTREE,
  INDEX `is_check`(`is_check`) USING BTREE,
  INDEX `kick_out`(`kick_out`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 124 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员认证表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_admins
-- ----------------------------
INSERT INTO `cnpscy_admins` VALUES (1, 'admin', '$2y$10$HyOTnVzzX3RhoCXlQ88qfumA81elPAPnBaORTWBSZT8Xg0s68ttKm', '202012/2r8hEvdSVBQa1s3sBCdcXt6fI57nVebNoaFaluQK.png', '123456@qq.com', '', 1, 2, 0, 0);
INSERT INTO `cnpscy_admins` VALUES (107, 'test', '$2y$10$gcgnFEWG47jf2JqM8onRp.OJrnB6JiMOP0.pClO0T1MbQPoo7ZB2C', '1', 'test@qq.com', '', 0, 2, 0, 0);
INSERT INTO `cnpscy_admins` VALUES (108, 'test1', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', '86', '123@qq.com', '', 0, 2, 0, 1);
INSERT INTO `cnpscy_admins` VALUES (109, 'test1', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', '86', '123@qq.com', '', 0, 2, 0, 1);
INSERT INTO `cnpscy_admins` VALUES (110, 'test1', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', '86', '123@qq.com', '', 0, 2, 0, 1);
INSERT INTO `cnpscy_admins` VALUES (111, 'test1', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', '86', '123@qq.com', '', 0, 2, 0, 1);
INSERT INTO `cnpscy_admins` VALUES (112, 'test1', '$2y$10$t6rC29.BfNtQc.s8DB8UkeNIQ3ZxjOP0u231oiTtiA2qQLdeOuumm', '86', '123@qq.com', '', 0, 2, 0, 1);
INSERT INTO `cnpscy_admins` VALUES (113, 'test', 'test1', '86', 'test@qq.com', '', 0, 2, 0, 1);
INSERT INTO `cnpscy_admins` VALUES (114, 'test1', '$2y$10$AeU9vwBx0d4KttM6yKcdWu8mgy2qgcTVK/v.I4MJuEzNlqz.Li9GG', '86', '123@qq.com', '', 0, 2, 0, 1);
INSERT INTO `cnpscy_admins` VALUES (117, 'test123', '$2y$10$lvVejUPm/lypn8Xr1YNHdOYdy7rrjqiv6DWsYlYAR4R4yidnLBg8e', '1', '123@qq.com', '', 0, 2, 0, 1);
INSERT INTO `cnpscy_admins` VALUES (118, 'test12311111', '$2y$10$5hSULbXycOJGQgU2p9ZGaepyIW9qG5DvG1BvyipESM.8WUf3rNKvy', '1', '123@qq.com', '', 0, 2, 0, 1);
INSERT INTO `cnpscy_admins` VALUES (119, 'test1232', '$2y$10$tnqyaBCn7HMwuellMuaH2OQq9ax1S1Wmnp7MAtBQ5Tf.dQ6a1fyeu', '1', '123@qq.com', '', 1, 2, 0, 1);
INSERT INTO `cnpscy_admins` VALUES (123, 'demo', '$2y$10$BSCJxQYvJAvfbQK2uOQNT.0SdzPvWc8GOBl09HADOYzo17hgW/rti', '202012/3yNDfZMiAV0rmOR0mhqkbmk3GWUgSOySgB6OCGVw.png', '43243242@qq.com', '', 1, 2, 0, 0);

-- ----------------------------
-- Table structure for cnpscy_adverts
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_adverts`;
CREATE TABLE `cnpscy_adverts`  (
  `advert_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '广告表',
  `advert_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `advert_cover` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '封面',
  `advert_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '外链',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`advert_id`) USING BTREE,
  INDEX `adverts_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_article_categories
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_article_categories`;
CREATE TABLE `cnpscy_article_categories`  (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级Id',
  `category_sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `is_check` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否可用：1：可用；0：禁用',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`category_id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `is_check`(`is_check`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 106 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_article_categories
-- ----------------------------
INSERT INTO `cnpscy_article_categories` VALUES (1, 'Taryn Halvorson', 2, 0, 1, 0, 1586760137, 1586846349);
INSERT INTO `cnpscy_article_categories` VALUES (2, 'Prof. Aurore Toy II', 0, 3, 1, 0, 1586760137, 1609315310);
INSERT INTO `cnpscy_article_categories` VALUES (3, 'Cleo Larson I', 0, 0, 0, 1, 1586760137, 1609315384);
INSERT INTO `cnpscy_article_categories` VALUES (4, 'Dr. Favian Bergstrom', 0, 0, 0, 1, 1586760137, 1609315393);
INSERT INTO `cnpscy_article_categories` VALUES (5, 'Rosa King', 0, 9, 0, 0, 1586760137, 1609747940);
INSERT INTO `cnpscy_article_categories` VALUES (6, 'Mr. Dave Hegmann DVM', 0, 0, 0, 1, 1586760137, 1609747932);
INSERT INTO `cnpscy_article_categories` VALUES (7, 'Penelope Kozey', 100, 0, 1, 0, 1586760137, 1609747948);
INSERT INTO `cnpscy_article_categories` VALUES (8, 'Martin Bogisich', 100, 3, 1, 0, 1586760137, 1609747961);
INSERT INTO `cnpscy_article_categories` VALUES (9, 'Meta Goldner', 0, 0, 0, 1, 1586760137, 1609747979);
INSERT INTO `cnpscy_article_categories` VALUES (10, 'Dr. Donavon Volkman', 0, 0, 1, 1, 1586760137, 1609747928);
INSERT INTO `cnpscy_article_categories` VALUES (11, 'Alexander Barrows', 100, 4, 1, 0, 1586760137, 1609747971);
INSERT INTO `cnpscy_article_categories` VALUES (12, 'Alisha Will IV', 0, 0, 1, 1, 1586760137, 1609748660);
INSERT INTO `cnpscy_article_categories` VALUES (13, 'Willow Haley', 0, 0, 1, 1, 1586760137, 1609728149);
INSERT INTO `cnpscy_article_categories` VALUES (14, 'Miss Berniece Wiza DDS', 0, 1, 1, 0, 1586760137, 1609728154);
INSERT INTO `cnpscy_article_categories` VALUES (15, 'Luna Rolfson DDS', 0, 0, 0, 0, 1586760137, 1609836485);
INSERT INTO `cnpscy_article_categories` VALUES (16, 'Doyle Botsford', 0, 0, 0, 0, 1586760137, 1609750506);
INSERT INTO `cnpscy_article_categories` VALUES (17, 'Prof. Adam Medhurst MD', 0, 0, 0, 0, 1586760137, 1609750506);
INSERT INTO `cnpscy_article_categories` VALUES (18, 'Dr. David Kessler I', 0, 0, 1, 1, 1586760137, 1609748006);
INSERT INTO `cnpscy_article_categories` VALUES (19, 'Dortha Hartmann', 0, 0, 0, 0, 1586760137, 1609750507);
INSERT INTO `cnpscy_article_categories` VALUES (20, 'Marisa Rempel', 0, 0, 0, 0, 1586760137, 1609750508);
INSERT INTO `cnpscy_article_categories` VALUES (21, 'Andy Schiller', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (22, 'Donato Langworth', 0, 0, 1, 1, 1586760137, 1609748022);
INSERT INTO `cnpscy_article_categories` VALUES (23, 'Cheyenne Herman', 0, 0, 1, 1, 1586760137, 1609748031);
INSERT INTO `cnpscy_article_categories` VALUES (24, 'Stevie Hermiston', 0, 0, 1, 1, 1586760137, 1609748019);
INSERT INTO `cnpscy_article_categories` VALUES (25, 'Prof. Ayla Kuvalis', 0, 0, 1, 1, 1586760137, 1609748011);
INSERT INTO `cnpscy_article_categories` VALUES (26, 'Mr. Irwin Zulauf', 0, 0, 1, 1, 1586760137, 1609748013);
INSERT INTO `cnpscy_article_categories` VALUES (27, 'Danielle Deckow', 0, 0, 1, 1, 1586760137, 1609748016);
INSERT INTO `cnpscy_article_categories` VALUES (28, 'Sandy Schimmel', 0, 0, 1, 1, 1586760137, 1609748495);
INSERT INTO `cnpscy_article_categories` VALUES (29, 'Prof. Cameron Hill', 100, 5, 1, 0, 1586760137, 1609748469);
INSERT INTO `cnpscy_article_categories` VALUES (30, 'Alanis Flatley', 0, 0, 1, 1, 1586760137, 1609748452);
INSERT INTO `cnpscy_article_categories` VALUES (31, 'Dr. Orpha Kohler', 0, 0, 1, 1, 1586760137, 1609748027);
INSERT INTO `cnpscy_article_categories` VALUES (32, 'Magali Marquardt', 0, 0, 1, 1, 1586760137, 1609748455);
INSERT INTO `cnpscy_article_categories` VALUES (33, 'Kameron Mills', 0, 0, 0, 1, 1586760137, 1609748489);
INSERT INTO `cnpscy_article_categories` VALUES (34, 'Susan White', 0, 0, 1, 1, 1586760137, 1609748458);
INSERT INTO `cnpscy_article_categories` VALUES (35, 'Telly Ward', 0, 0, 1, 1, 1586760137, 1609748492);
INSERT INTO `cnpscy_article_categories` VALUES (36, 'Ms. Albina Schmeler', 0, 0, 1, 1, 1586760137, 1609748498);
INSERT INTO `cnpscy_article_categories` VALUES (37, 'Prof. Cathryn Swaniawski V', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (38, 'Valentin Leannon III', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (39, 'Hermina Bruen', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (40, 'Andy Fadel', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (41, 'Houston Connelly', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (42, 'Lysanne Hauck', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (43, 'Marta Becker', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (44, 'Mr. Reagan Nader DVM', 0, 0, 1, 1, 1586760137, 1609747886);
INSERT INTO `cnpscy_article_categories` VALUES (45, 'Carlo Schmitt', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (46, 'Mr. Reyes Heaney', 0, 0, 1, 1, 1586760137, 1609747883);
INSERT INTO `cnpscy_article_categories` VALUES (47, 'Zena Leffler', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (48, 'Dr. Gerhard Erdman', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (49, 'Melvin Feil', 0, 0, 1, 1, 1586760137, 1609747880);
INSERT INTO `cnpscy_article_categories` VALUES (50, 'Mr. Maximilian Stracke DVM', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (51, 'Sophie Schultz IV', 0, 0, 1, 1, 1586760137, 1609747890);
INSERT INTO `cnpscy_article_categories` VALUES (52, 'Heather Dietrich', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (53, 'Adriel Jacobs', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (54, 'Mrs. Margarett Kihn Jr.', 0, 0, 1, 1, 1586760137, 1609747893);
INSERT INTO `cnpscy_article_categories` VALUES (55, 'Ms. Shanelle Lesch V', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (56, 'Nikita Weber', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (57, 'Sallie Cormier', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (58, 'Easter Smitham', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (59, 'Aron Dooley', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (60, 'Ariane Aufderhar', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (61, 'Dr. Martin Greenfelder', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (62, 'Dr. Darion Sanford DDS', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (63, 'Margret Smitham', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (64, 'Clinton McDermott MD', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (65, 'Raymond Weimann V', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (66, 'Alicia Volkman', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (67, 'Camren Rolfson', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (68, 'Lenore Ledner', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categories` VALUES (69, 'Shyann Wyman', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (70, 'Verlie Osinski V', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (71, 'Agnes Haag', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (72, 'Dr. Clint Reilly', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (73, 'Zelda Runolfsdottir DVM', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (74, 'Minerva Murazik', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (75, 'Prof. Courtney Hoppe', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (76, 'Barry Price', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (77, 'Brenda Schmidt', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (78, 'Emmanuel Greenfelder III', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (79, 'Bridget Stanton', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (80, 'Kory Rolfson', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (81, 'Ophelia Carter', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (82, 'Joanne Trantow V', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (83, 'Jerel Mann', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (84, 'Dr. Johnson Bruen', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (85, 'Beverly Johns DVM', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (86, 'Jarrett Oberbrunner', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (87, 'Miss Loma Carroll V', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (88, 'Miss Sandra Boehm Jr.', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (89, 'Miracle Nolan DVM', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (90, 'Helene Boyle', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (91, 'Denis Zieme', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (92, 'Hortense Dicki', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (93, 'Keon Gislason DDS', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (94, 'Keshawn Friesen', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categories` VALUES (95, 'Patricia Volkman', 0, 0, 0, 0, 1586760138, 1609898815);
INSERT INTO `cnpscy_article_categories` VALUES (96, 'Trace Lang PhD', 0, 0, 1, 0, 1586760138, 1609836405);
INSERT INTO `cnpscy_article_categories` VALUES (97, 'Mr. Brennan Tremblay MD', 0, 0, 1, 0, 1586760138, 1609901617);
INSERT INTO `cnpscy_article_categories` VALUES (98, 'Kristian Nolan', 0, 0, 0, 1, 1586760138, 1609747982);
INSERT INTO `cnpscy_article_categories` VALUES (99, 'Amara Glover', 0, 0, 0, 1, 1586760138, 1609901615);
INSERT INTO `cnpscy_article_categories` VALUES (100, 'Mrs. Odie Brekke IV', 0, 0, 0, 0, 1586760138, 1609836485);
INSERT INTO `cnpscy_article_categories` VALUES (101, '二级分类', 100, 0, 1, 0, 1586763466, 1586763466);
INSERT INTO `cnpscy_article_categories` VALUES (102, '测试', 0, 99, 1, 0, 1609317234, 1609317234);
INSERT INTO `cnpscy_article_categories` VALUES (103, '新增-API', 0, 99, 1, 0, 1609732743, 1609732743);
INSERT INTO `cnpscy_article_categories` VALUES (104, '312321321', 100, 99, 1, 0, 1609748509, 1609748509);
INSERT INTO `cnpscy_article_categories` VALUES (105, '111113333', 95, 99, 0, 1, 1609825196, 1609825214);

-- ----------------------------
-- Table structure for cnpscy_article_categorys
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_article_categorys`;
CREATE TABLE `cnpscy_article_categorys`  (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级Id',
  `category_sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `is_check` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否可用：1：可用；0：禁用',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`category_id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `is_check`(`is_check`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_article_categorys
-- ----------------------------
INSERT INTO `cnpscy_article_categorys` VALUES (1, 'Taryn Halvorson', 2, 0, 1, 0, 1586760137, 1586846349);
INSERT INTO `cnpscy_article_categorys` VALUES (2, 'Prof. Aurore Toy II', 0, 0, 0, 0, 1586760137, 1586916236);
INSERT INTO `cnpscy_article_categorys` VALUES (3, 'Cleo Larson I', 0, 0, 0, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (4, 'Dr. Favian Bergstrom', 0, 0, 0, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (5, 'Rosa King', 0, 0, 0, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (6, 'Mr. Dave Hegmann DVM', 0, 0, 0, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (7, 'Penelope Kozey', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (8, 'Martin Bogisich', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (9, 'Meta Goldner', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (10, 'Dr. Donavon Volkman', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (11, 'Alexander Barrows', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (12, 'Alisha Will IV', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (13, 'Willow Haley', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (14, 'Miss Berniece Wiza DDS', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (15, 'Luna Rolfson DDS', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (16, 'Doyle Botsford', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (17, 'Prof. Adam Medhurst MD', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (18, 'Dr. David Kessler I', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (19, 'Dortha Hartmann', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (20, 'Marisa Rempel', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (21, 'Andy Schiller', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (22, 'Donato Langworth', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (23, 'Cheyenne Herman', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (24, 'Stevie Hermiston', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (25, 'Prof. Ayla Kuvalis', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (26, 'Mr. Irwin Zulauf', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (27, 'Danielle Deckow', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (28, 'Sandy Schimmel', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (29, 'Prof. Cameron Hill', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (30, 'Alanis Flatley', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (31, 'Dr. Orpha Kohler', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (32, 'Magali Marquardt', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (33, 'Kameron Mills', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (34, 'Susan White', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (35, 'Telly Ward', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (36, 'Ms. Albina Schmeler', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (37, 'Prof. Cathryn Swaniawski V', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (38, 'Valentin Leannon III', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (39, 'Hermina Bruen', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (40, 'Andy Fadel', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (41, 'Houston Connelly', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (42, 'Lysanne Hauck', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (43, 'Marta Becker', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (44, 'Mr. Reagan Nader DVM', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (45, 'Carlo Schmitt', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (46, 'Mr. Reyes Heaney', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (47, 'Zena Leffler', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (48, 'Dr. Gerhard Erdman', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (49, 'Melvin Feil', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (50, 'Mr. Maximilian Stracke DVM', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (51, 'Sophie Schultz IV', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (52, 'Heather Dietrich', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (53, 'Adriel Jacobs', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (54, 'Mrs. Margarett Kihn Jr.', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (55, 'Ms. Shanelle Lesch V', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (56, 'Nikita Weber', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (57, 'Sallie Cormier', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (58, 'Easter Smitham', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (59, 'Aron Dooley', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (60, 'Ariane Aufderhar', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (61, 'Dr. Martin Greenfelder', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (62, 'Dr. Darion Sanford DDS', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (63, 'Margret Smitham', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (64, 'Clinton McDermott MD', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (65, 'Raymond Weimann V', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (66, 'Alicia Volkman', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (67, 'Camren Rolfson', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (68, 'Lenore Ledner', 0, 0, 1, 0, 1586760137, 1586760137);
INSERT INTO `cnpscy_article_categorys` VALUES (69, 'Shyann Wyman', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (70, 'Verlie Osinski V', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (71, 'Agnes Haag', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (72, 'Dr. Clint Reilly', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (73, 'Zelda Runolfsdottir DVM', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (74, 'Minerva Murazik', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (75, 'Prof. Courtney Hoppe', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (76, 'Barry Price', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (77, 'Brenda Schmidt', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (78, 'Emmanuel Greenfelder III', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (79, 'Bridget Stanton', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (80, 'Kory Rolfson', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (81, 'Ophelia Carter', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (82, 'Joanne Trantow V', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (83, 'Jerel Mann', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (84, 'Dr. Johnson Bruen', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (85, 'Beverly Johns DVM', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (86, 'Jarrett Oberbrunner', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (87, 'Miss Loma Carroll V', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (88, 'Miss Sandra Boehm Jr.', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (89, 'Miracle Nolan DVM', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (90, 'Helene Boyle', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (91, 'Denis Zieme', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (92, 'Hortense Dicki', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (93, 'Keon Gislason DDS', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (94, 'Keshawn Friesen', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (95, 'Patricia Volkman', 0, 0, 0, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (96, 'Trace Lang PhD', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (97, 'Mr. Brennan Tremblay MD', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (98, 'Kristian Nolan', 0, 0, 0, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (99, 'Amara Glover', 0, 0, 1, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (100, 'Mrs. Odie Brekke IV', 0, 0, 0, 0, 1586760138, 1586760138);
INSERT INTO `cnpscy_article_categorys` VALUES (101, '二级分类', 100, 0, 1, 0, 1586763466, 1586763466);

-- ----------------------------
-- Table structure for cnpscy_article_contents
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_article_contents`;
CREATE TABLE `cnpscy_article_contents`  (
  `article_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章Id',
  `article_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '内容',
  INDEX `article_contents_article_id_index`(`article_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_article_contents
-- ----------------------------
INSERT INTO `cnpscy_article_contents` VALUES (123, '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; font-weight: 700; color: rgb(102, 102, 102); font-family: \"><a href=\"http://www.xiami.com/lrc/1795428369?spm=a1z1s.3061781.0.0.gfanxx\" style=\"box-sizing: border-box; color: rgb(102, 102, 102); text-decoration-line: none; border: 0px; margin: 0px; outline: 0px; padding: 0px; cursor: pointer; -webkit-tap-highlight-color: rgb(254, 150, 0);\"></a></span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; font-weight: 700; color: rgb(102, 102, 102); font-family: \">《光年之外》歌词：</span><span style=\"box-sizing: border-box; color: rgb(102, 102, 102); font-family: \"></span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57);\">感受停在我发端的指尖<br>如何瞬间冻结时间<br>记住望着我坚定的双眼<br>也许已经没有明天<br>面对浩瀚的星海<br>我们微小得像尘埃<br>漂浮在一片无奈<br>缘分让我们相遇乱世以外<br>命运却要我们危难中相爱<br>也许未来遥远在光年之外<br>我愿守候未知里为你等待<br>我没想到为了你我能疯狂到<br>山崩海啸没有你根本不想逃<br>我的大脑为了你已经疯狂到<br>脉搏心跳没有你根本不重要<br><br>一双围在我胸口的臂弯<br>足够抵挡天旋地转<br>一种执迷不放手的倔强<br>足以点燃所有希望<br>宇宙磅礡而冷漠<br>我们的爱微小却闪烁<br>颠簸却如此忘我<br>缘分让我们相遇乱世以外<br>命运却要我们危难中相爱<br>也许未来遥远在光年之外<br>我愿守候未知里为你等待<br>我没想到为了你我能疯狂到<br>山崩海啸没有你根本不想逃<br>我的大脑为了你已经疯狂到<br>脉搏心跳没有你根本不重要<br>也许航道以外是醒不来的梦<br>乱世以外是纯粹的相拥<br>我没想到为了你我能疯狂到<br>山崩海啸没有你根本不想逃<br>我的大脑为了你已经疯狂到<br>脉搏心跳没有你根本不重要<br>相遇乱世以外危难中相爱<br>相遇乱世以外危难中相爱<br>我没想到</p>');
INSERT INTO `cnpscy_article_contents` VALUES (124, '<p>噗哈哈<img src=\"http://img.baidu.com/hi/jx2/j_0071.gif\">完美搞定</p>\n<p><img id=\"avatar\" class=\"editable img-responsive editable-click editable-empty\" alt=\"mysql主从配置\" src=\"/storage/admin/160753_1_682106.jpg\">    <br></p>\n<p><br></p>\n<p><br></p>');
INSERT INTO `cnpscy_article_contents` VALUES (125, '<p>会出现这种原因：</p>\n<p>    1.开启了时间戳自动更新 $timestamps = true;</p>\n<p>    2.并且定义了CREATED_AT UPDATED_AT【更改创建时间与更新时间为自己习惯的字段名称】；</p>\n<p>只有上两步同时使用时，才会出现这种问题。</p>\n<p><br></p>\n<p><br></p>\n<p>位置：\\vendor\\illuminate\\database\\Eloquent\\Concerns\\HasAttributes.php</p>\n<ol class=\" list-paddingleft-2\" style=\"list-style-type: decimal;\">\n<li><p>1.要么在模型中不要使用 $timestamps = true;</p></li>\n<li><p>2.要么在模型中不要定义 CREATED_AT UPDATED_AT；</p></li>\n<li><p>3.在模型中，定义访问器【get字段名Attribute】；<br></p></li>\n<li><p>4.要么不要在模型中查询【如果在控制器查询，不会出现这种问题】；</p></li>\n<li><p>5.要么注释掉【最不推荐的一种】。<br></p></li>\n</ol>\n<p style=\"margin-bottom: 5px;\"><img id=\"avatar\" class=\"editable img-responsive editable-click editable-empty\" alt=\"图片上传\" src=\"/storage/admin/173843_1_591847.jpg\">    <img id=\"avatar\" class=\"editable img-responsive editable-click editable-empty\" alt=\"图片上传\" src=\"/storage/admin/174414_1_448955.jpg\"></p>');
INSERT INTO `cnpscy_article_contents` VALUES (126, '<p style=\"text-align: center;\"><span style=\"font-size: 24px;\"><em><strong>第三方登录与支付宝支付。</strong></em></span><img src=\"http://blog.cnpscy.cn/storage/_ueditor/image/20190215/1550224610394766.png\" title=\"1550224610394766.png\"><img src=\"/storage/_ueditor/image/20190215/1550224610298403.png\" title=\"1550224610298403.png\"></p>\n<p><br></p>\n<p><img src=\"/storage/_ueditor/image/20190215/1550224610123485.jpg\" title=\"1550224610123485.jpg\"></p>\n<p><img src=\"/storage/_ueditor/image/20190215/1550224610576385.png\" title=\"1550224610576385.png\"></p>\n<p><img src=\"/storage/_ueditor/image/20190215/1550224611261035.png\" title=\"1550224611261035.png\"></p>\n<p><img src=\"/storage/_ueditor/image/20190215/1550224611719714.png\" title=\"1550224611719714.png\"></p>\n<p><img src=\"/storage/_ueditor/image/20190215/1550224611562686.png\" title=\"1550224611562686.png\"></p>\n<p><br></p>');
INSERT INTO `cnpscy_article_contents` VALUES (127, '<p></p>\n<h1 style=\"text-align: center;\">\n<span style=\"font-family: 楷体, 楷体_GB2312, SimKai; font-size: 36px;\"><strong>下载地址</strong>：</span><a href=\"https://newsapp.cnpscy.cn/cnpscy-newsAPP-2019.6.26.apk\" target=\"_blank\" title=\"小丑路人-新闻资讯APP\" style=\"text-decoration: underline; font-family: 楷体, 楷体_GB2312, SimKai; font-size: 36px;\"><span style=\"font-family: 楷体, 楷体_GB2312, SimKai; font-size: 36px;\">小丑路人-新闻资讯APP</span></a>\n</h1>\n<p><br></p>\n<p><img src=\"/storage/_ueditor/image/20190626/1561561833827555.jpg\" title=\"1561561833827555.jpg\" alt=\"Screenshot_20190626-214647_ - APP.jpg\">  <img src=\"/storage/_ueditor/image/20190626/1561561842978577.jpg\" title=\"1561561842978577.jpg\" alt=\"Screenshot_20190626-214657_ - APP.jpg\">  <img src=\"/storage/_ueditor/image/20190626/1561561850996905.jpg\" title=\"1561561850996905.jpg\" alt=\"Screenshot_20190626-214704_ - APP.jpg\">  <img src=\"/storage/_ueditor/image/20190626/1561561852590580.jpg\" title=\"1561561852590580.jpg\" alt=\"Screenshot_20190626-214711_ - APP.jpg\"></p>');
INSERT INTO `cnpscy_article_contents` VALUES (128, '<p><br></p>\n<p>文档：Swoole环境安装.md</p>\n<p>链接：</p>\n<p><a href=\"http://note.youdao.com/noteshare?id=951cde2e6229d11fdd8f0d94384e64f9&amp;sub=597BB270C80B480F8426D275CF4CA12F\">http://note.youdao.com/noteshare?id=951cde2e6229d11fdd8f0d94384e64f9&amp;sub=597BB270C80B480F8426D275CF4CA12F</a></p>\n<p><br></p>\n<article><pre>\r\n    服务器系统：centos 7\r\n    \r\n\r\n\r\n    升级所有包同时也升级软件和系统内核\r\n    yum update    \r\n\r\n## PHP7.2安装：\r\n    1.安装源\r\n\r\n        安装php72w，是需要配置额外的yum源地址的，否则会报错不能找到相关软件包。\r\n    \r\n        php高版本的yum源地址，有两部分，其中一部分是epel-release，另外一部分来自webtatic。如果跳过epel-release的话，安装webtatic的时候，会有错误爆出。\r\n        \r\n        所以，这里需要的命令是：\r\n        \r\n        rpm -Uvh https://dl.Fedoraproject.org/pub/epel/7/x86_64/Packages/e/epel-release-7-11.noarch.rpm\r\n        \r\n        rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm\r\n        \r\n        当然，您也可以选择下面的这个命令，也是一样的效果。\r\n        \r\n        yum install epel-release -y\r\n        \r\n        rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm\r\n        \r\n    2、清除历史版本\r\n    \r\n        为了防止CentOS上面发生php冲突，所以，这个命令还是先执行一下更好些。\r\n        \r\n        yum -y remove php*\r\n        \r\n    3、安装扩展包\r\n    \r\n        事实上，这里面的对应扩展库很多，这里大家一定要注意cli和fpm这两个包，而其它的相关包就看您需要了。\r\n        \r\n        yum -y install php72w php72w-cli php72w-fpm php72w-common php72w-devel\r\n        \r\n        还有比较豪华的版本：\r\n        \r\n        yum -y install php72w php72w-cli php72w-fpm php72w-common php72w-devel php72w-embedded php72w-gd php72w-mbstring php72w-mysqlnd php72w-opcache php72w-pdo php72w-xml\r\n    \r\n    4.检测PHP是否安装成功\r\n        php -v\r\n        \r\n        PHP 7.2.24 (cli) (built: Oct 26 2019 12:28:19) ( NTS )\r\n        Copyright (c) 1997-2018 The PHP Group \r\n        Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies\r\n        with Zend OPcache v7.2.24, Copyright (c) 1999-2018, by Zend Technologies\r\n    \r\n## 安装gcc、c++编译器以及内核文件\r\n    \r\n    sudo yum -y install gcc gcc-c++ kernel-devel\r\n    \r\n## 如果没有安装make：\r\n\r\n    yum install make\r\n    \r\n## 安装autoconf:\r\n\r\n    yum install autoconf\r\n    \r\n## 安装pcre:\r\n    \r\n    yum install pcre-devel\r\n    \r\n## Swoole的安装编译：\r\n    \r\n    下载地址： https://github.com/swoole/swoole-src/releases\r\n    \r\n    下载压缩包\r\n    wget https://github.com/swoole/swoole-src/archive/v4.4.12.tar.gz\r\n    \r\n    ls 查看文件列表\r\n    \r\n    解压压缩包\r\n    tar -zxvf v4.4.12.tar.gz\r\n    \r\n    进入文件\r\n    cd swoole-src-4.4.12/\r\n    \r\n    Swoole编译示例： https://wiki.swoole.com/wiki/page/6.html\r\n    \r\n    phpize\r\n    ./configure\r\n    make \r\n    sudo make install\r\n    \r\n    \r\n## 查看php.ini的位置:\r\n    php -i | grep php.ini\r\n    \r\n## 配置php.ini\r\n    打开文件\r\n    vim /etc/php.ini\r\n    \r\n    esc -&gt; :w \r\n    \r\n    最后加入\r\n    extension=swoole.so\r\n    \r\n    esc -&gt; :wq!\r\n    保存成功！\r\n\r\n## 检测是否加载swoole\r\n    php -m</pre></article>');
INSERT INTO `cnpscy_article_contents` VALUES (129, '<p>码云：<a href=\"https://gitee.com/cnpscy/laravel6-openSource-basic-admin\">https://gitee.com/cnpscy/laravel6-openSource-basic-admin</a></p>\n<p><br></p>\n<p><img src=\"/storage/ueditor/image/20191227/1577429440631766.png\" title=\"1577429440631766.png\" alt=\"QQ图片20191227145008.png\"></p>');
INSERT INTO `cnpscy_article_contents` VALUES (130, '<p><img src=\"/storage/_ueditor/image/20181117/1542447645.png\" title=\"1542447645.png\" alt=\"222222222222226.png\"></p>');
INSERT INTO `cnpscy_article_contents` VALUES (131, '<p style=\"text-align: left;\">在bootstrap/app.php文件内部：</p>\n<p></p>\n<hr>\n<p>$app = new Laravel\\Lumen\\Application(</p>\n<p>    dirname(__DIR__)</p>\n<p>);</p>\n<p><br></p>\n<pre class=\"brush:php;toolbar:false\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); white-space: pre-wrap; overflow-wrap: break-word; color: rgb(85, 85, 85); font-size: 15px; background-color: rgb(255, 255, 255);\">--------下方的任意位置---------</pre>\n<p><br></p>\n<p>/**</p>\n<p> * 软连接 不存在，默认生成</p>\n<p> */</p>\n<p>if (!file_exists($app-&gt;basePath(\'public/storage\'))) {</p>\n<p>    $app-&gt;make(\'files\')-&gt;link(</p>\n<p>        storage_path(\'app/public\'), $app-&gt;basePath(\'public/storage\')</p>\n<p>    );</p>\n<p>}</p>\n<p><br></p>\n<pre class=\"brush:php;toolbar:false\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); white-space: pre-wrap; overflow-wrap: break-word; color: rgb(85, 85, 85); font-size: 15px; background-color: rgb(255, 255, 255);\">--------上方的任意位置---------</pre>\n<p><br></p>\n<p>return $app;</p>\n<p><br></p>\n<hr>\n<p>插入即可。</p>');
INSERT INTO `cnpscy_article_contents` VALUES (132, '<p><span style=\"color: rgb(217, 150, 148); font-family: punctuation, \">人生自古谁无情，只是花开花落人伤情！</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (133, '<p><span style=\"color: rgb(247, 150, 70); font-family: 宋体, Arial, Helvetica, sans-serif; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">人生在世虚几度，风貌年华似流水。</span><br><span style=\"color: rgb(247, 150, 70); font-family: 宋体, Arial, Helvetica, sans-serif; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">坠入魔道非吾愿，早已事非人已非。</span><br><span style=\"color: rgb(247, 150, 70); font-family: 宋体, Arial, Helvetica, sans-serif; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">此生此世虚何年，不枉今朝再化人。</span><br><span style=\"color: rgb(247, 150, 70); font-family: 宋体, Arial, Helvetica, sans-serif; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">不争人间是与非，一世情缘现诺言。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (134, '<p><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">潇潇几何耐多愁，枯木坦何消与愁。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">只为借酒来清愁，化作酒杯气断流。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">安居乐业多知音，铁战沙场出豪杰。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">借酒一杯来消愁，美酒佳肴忆往昔。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (135, '<p><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; background-color: rgb(255, 255, 255);\">深呼吸，浅惆怅，时光易老，岁月无情。无奈人生身是客，何多愁，耐九寒，天地无悠悠然也。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (136, '<p><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-indent: 28px; background-color: rgb(255, 255, 255);\">萧何一段沧桑感，情缘一朝世轮回。 我愿只身度千年，只求一世一轮回。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (137, '<p><span style=\"margin: 0px; padding: 0px; text-align: center; text-indent: 28px; background-color: rgb(255, 255, 255); color: rgb(112, 48, 160); font-family: 幼圆; white-space: pre-wrap; font-size: 24px;\">欢与散，一念间。情与缘，一世恋。人与情，一相遇。我与你，不可求。</span><span style=\"margin: 0px; padding: 0px; text-align: center; text-indent: 28px; background-color: rgb(255, 255, 255); font-size: 24px; font-family: 幼圆; color: rgb(112, 48, 160);\"> </span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (138, '<p><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">千古愁，万古倡。分相离，恨别惜。引牵曦，落日霞。一场梦，诧醒间。不回手，相结伴，与君行。行可易，难同行，分离间。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (139, '<ul class=\"infos list-paddingleft-2\" style=\"list-style-type: none;\">\n<p style=\"margin-top: 0px; margin-bottom: 20px; padding: 0px; color: rgb(112, 48, 160); font-family: 幼圆; text-align: center;\"><span style=\"margin: 0px 0px 0px -23px; padding: 0px; font-size: 24px;\">你我本已无缘，又何必曾相识。</span></p>\n<p style=\"margin-top: 0px; margin-bottom: 20px; padding: 0px; color: rgb(112, 48, 160); font-family: 幼圆; text-align: center;\"><span style=\"margin: 0px 0px 0px -23px; padding: 0px; font-size: 24px;\">只为擦肩而过，无须任何留恋。</span></p>\n<p style=\"margin-top: 0px; margin-bottom: 20px; padding: 0px; color: rgb(112, 48, 160); font-family: 幼圆; text-align: center;\"><span style=\"margin: 0px 0px 0px -23px; padding: 0px; font-size: 24px;\">你我擦肩而过，只为回眸一笑。</span></p>\n<p style=\"margin-top: 0px; margin-bottom: 20px; padding: 0px; color: rgb(112, 48, 160); font-family: 幼圆; text-align: center;\"><span style=\"margin: 0px 0px 0px -23px; padding: 0px; font-size: 24px;\">各自转身离去，便是天涯一方。</span></p>\n<p style=\"margin-top: 0px; margin-bottom: 20px; padding: 0px; color: rgb(112, 48, 160); font-family: 幼圆; text-align: center;\"><span style=\"margin: 0px 0px 0px -23px; padding: 0px; font-size: 24px;\">不再相遇相见，便已不曾相识。</span></p>\n</ul>\n<p><br></p>');
INSERT INTO `cnpscy_article_contents` VALUES (140, '<p><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">潇潇洒洒一解脱，和和睦睦万家亲。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">萧何一段问沧桑，沧桑一感别天涯。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">何时何断感无痕，执手一笔落情殇。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">弃守花田一月下，零落时节月下魂。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (141, '<p style=\"text-align: center;\"><span style=\" padding: 0px; text-align: center; background-color: rgb(255, 255, 255); color: rgb(112, 48, 160); font-family: 幼圆; white-space: pre-wrap; font-size: 24px;\">潇潇洒洒一解脱，和和睦睦万家亲。<br>萧何一段问沧桑，沧桑一感别天涯。<br>何时何断感无痕，执手一笔落情殇。<br>弃守花田一月下，零落时节月下魂。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (142, '<p><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; text-indent: 28px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">情已散，念已断 ；雨水无痕，落遍心川 。无缘续情愁！潇潇泪痕无以断， 瑟瑟落幕情痕愁。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (143, '<p style=\"text-align: center;\"><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">萧风吹过一浪起，浪花溅飞一双飞。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">千言万语一字攥，始终不离一话题！</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">一句一怨铩情归，便是当时情散了。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">一风一叶一束情，万缘情愁终归散。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (144, '<p style=\"text-align: center;\"><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">潇潇瑟瑟万事川，跨河一渡临咫尺。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">欲行欲止终止步，无欲行过仅一眸。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">只求时光止于刻，相视一笑一千年。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">霜花飞舞飘过客，芳香滞留此倾世。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (145, '<p><span style=\"color: rgb(41, 50, 51); font-family: punctuation, \">        有时候，睡觉是一种享受，吃饭是一种考验，打球是一种体验，上课是一种煎熬，离开是一种必然，两情相悦是一种猜想，擦肩而过是一种守候，回眸一笑是一种姿态，背影是一种丑态，放弃是一种失败，微笑是一种现实。微笑是一种礼貌。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (146, '<p style=\"text-align: center;\"><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">一线天外呈人间，无缘再做有情人。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">只愿化作一尘土，飘荡人间落何处。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">缘起缘灭缘飞渡，化作尘埃一尘世。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">风云乱世现枭雄，一花一草一世间。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (147, '<p style=\"text-align: center;\"><span style=\"margin: 0px; padding: 0px; text-align: center; background-color: rgb(255, 255, 255); font-size: 24px; font-family: 幼圆; color: rgb(112, 48, 160);\">清目柳眉壁如玉，细水长发飘若间。<br>硃红耳哧集一身，气质颜容非人间。<br>谈笑间一世倾城，回眸一笑百媚生。<br></span><span style=\"margin: 0px; padding: 0px; font-family: punctuation, 微软雅黑, Tohoma; font-size: 14px; text-align: center; background-color: rgb(255, 255, 255);\"></span><span style=\"margin: 0px; padding: 0px; text-align: center; background-color: rgb(255, 255, 255); font-size: 24px; font-family: 幼圆; color: rgb(112, 48, 160);\">万劫众缘终归散，一气呵成千古颂。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (148, '<p style=\"text-align: center;\"><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">冰清玉洁万事开，零落萧何终归闭。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">斜劈刘海洒清风，万事终究随风去。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\">流风一去不复返，此却只留无痕迹。</span><br><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; text-align: center; background-color: rgb(255, 255, 255);\"> 空无人际飘荡处，且剩相遇那一刻。 </span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (149, '<p style=\"text-align: center;\"><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; background-color: rgb(255, 255, 255);\">飘洒落无情，雨夜终归土。</span></p>\n<p style=\"text-align: center;\"><span style=\"color: rgb(112, 48, 160); font-family: 幼圆; font-size: 24px; background-color: rgb(255, 255, 255);\">  万千丝发愁，歌舞戏游人。  </span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (150, '<p><span style=\"color: rgb(41, 50, 51); font-family: punctuation, \">忘乎所以，念汝鹊巢临馨遇；忘情所景，观汝眼体其心，汝当零落飘洒一刻间，凌若兮。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (151, '<ul class=\"infos list-paddingleft-2\" style=\"list-style-type: none;\">\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW DATABASES                                //列出 MySQL Server 数据库。</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW TABLES [FROM db_name]                    //列出数据库数据表。</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW CREATE TABLES tbl_name                    //导出数据表结构。</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW TABLE STATUS [FROM db_name]              //列出数据表及表状态信息。</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW COLUMNS FROM tbl_name [FROM db_name]     //列出资料表字段</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW FIELDS FROM tbl_name [FROM db_name]，DESCRIBE tbl_name [col_name]。</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW FULL COLUMNS FROM tbl_name [FROM db_name]//列出字段及详情</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW FULL FIELDS FROM tbl_name [FROM db_name] //列出字段完整属性</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW INDEX FROM tbl_name [FROM db_name]       //列出表索引。</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW STATUS                                  //列出 DB Server 状态。</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW VARIABLES                               //列出 MySQL 系统环境变量。</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW PROCESSLIST                             //列出执行命令。</p>\n<p style=\"margin-top: 0px; margin-bottom: 10px; padding: 0px; box-sizing: border-box; color: rgb(57, 57, 57); font-family: \">SHOW GRANTS FOR user                         //列出某用户权限</p>\n</ul>\n<p><br></p>');
INSERT INTO `cnpscy_article_contents` VALUES (152, '<p><span style=\"color: rgb(57, 57, 57); font-family: punctuation, 微软雅黑, Tohoma; font-size: 14px; text-indent: 28px; background-color: rgb(255, 255, 255);\">group_concat(title)    --- 查询的记录，同一用户或者相同的某个字段为前提时，title 以字符串形式合并为一条数据。</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (153, '<p><span style=\"box-sizing: border-box; font-family: \">梦境迷糊不定，深不可测。</span><img alt=\"☺\" class=\"emoticon\" src=\"/storage/_ueditor/image/20181117/1542446754.gif\"></p>');
INSERT INTO `cnpscy_article_contents` VALUES (154, '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px;\">1、网站技术架构</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px;\">2、rtmp推流</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px;\">3、高并发</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px;\">4、直播视频采集SDK</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px;\">5、数据采集→数据编码→数据传输(流媒体服务器) →解码数据→播放显示</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px;\">6、RTMP(Real Time Messaging Protocol，实时消息传送协议)</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px;\">7、码率（bit rate）、分辨率（resolution）、帧率（frame rate）、丢包率（packet loss rate）、时延（delay）</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px;\">8、消息推送（针对关注的用户推送关注的直播人）</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px;\">......</p>\n<p><br></p>');
INSERT INTO `cnpscy_article_contents` VALUES (155, '<p><span style=\"color: rgb(57, 57, 57); font-family: \">已萧然，殊不知沉封入雪，理缳乱，人枉然！</span></p>');
INSERT INTO `cnpscy_article_contents` VALUES (156, '<p><img src=\"/storage/_ueditor/image/20181117/1542447010.jpg\" title=\"1542447010.jpg\" alt=\"apicloud-1.jpg\"><img src=\"/storage/_ueditor/image/20181117/1542447019.jpg\" title=\"1542447019.jpg\" alt=\"apicloud-2.jpg\"><img src=\"/storage/_ueditor/image/20181117/1542447021.jpg\" title=\"1542447021.jpg\" alt=\"apicloud-3.jpg\"></p>');
INSERT INTO `cnpscy_article_contents` VALUES (157, '<p><img src=\"http://img.baidu.com/hi/jx2/j_0004.gif\">离职了一个月，终于跑路了；</p>\n<p>MMP，但是，还是要把之前公司的项目给做完才行，这不坑爹了吗！我擦嘞</p>\n<p><br></p>\n<p>最后我的一个月工资还没发我（长沙天二网络）</p>');
INSERT INTO `cnpscy_article_contents` VALUES (158, '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><strong style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; color: rgb(255, 0, 0); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 20px;\">更换 apt-get 源<br></span></strong></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><strong style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; font-family: Verdana, Arial, Helvetica, sans-serif; color: rgb(255, 0, 0); font-size: 20px;\">cd /etc/apt/sources.list</span></strong></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;\">内容全部更换为：</span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">deb http://ftp.debian.org/debian jessie main contrib non-free</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">deb http://ftp.debian.org/debian jessie-updates main contrib non-free</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">deb http://security.debian.org jessie/updates main contrib non-free</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">deb http://packages.dotdeb.org jessie all</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">deb http://ftp.debian.org/debian jessie-backports mai</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">deb http://packages.dotdeb.org jessie all</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">deb-src http://packages.dotdeb.org jessie all</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">运行升级命令来确保我们的系统组件各方面都是最新的。</p>\n<blockquote style=\"box-sizing: border-box; padding: 5px 20px; margin: 15px 0px; border-width: 1px 1px 1px 8px; border-style: solid; border-color: rgb(221, 221, 221); font-family: \"><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">apt-get update<br>apt-get upgrade --show-upgraded</p></blockquote>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \"><span style=\"box-sizing: border-box; font-size: 20px; color: rgb(255, 0, 0);\"><strong style=\"box-sizing: border-box;\">一、安装Apache Web服务器的当前版</strong><strong style=\"box-sizing: border-box;\">本（在2.x系列），执行命令如下：</strong></span></p>\n<blockquote style=\"box-sizing: border-box; padding: 5px 20px; margin: 15px 0px; border-width: 1px 1px 1px 8px; border-style: solid; border-color: rgb(221, 221, 221); font-family: \"><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">apt-get install apache2</p></blockquote>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \"><span style=\"box-sizing: border-box; font-family: Verdana, Arial, Helvetica, sans-serif;\">安装好后开服务</span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \"><span style=\"box-sizing: border-box; font-family: Verdana, Arial, Helvetica, sans-serif;\"></span></p>\n<blockquote style=\"box-sizing: border-box; padding: 5px 20px; margin: 15px 0px; border-width: 1px 1px 1px 8px; border-style: solid; border-color: rgb(221, 221, 221); font-family: \"><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">service apache2 start</p></blockquote>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">展示页面为：<span style=\"box-sizing: border-box; font-family: Verdana, Arial, Helvetica, sans-serif;\"></span><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \"><img src=\"/storage/_ueditor/image/20181117/1542447527.png\" title=\"1505398679991064.png\" alt=\"QQ图片20170914221749.png\"></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \"><span style=\"box-sizing: border-box; font-size: 20px; color: rgb(255, 0, 0);\"><strong style=\"box-sizing: border-box;\">二、开始安装PHP7.0</strong></span><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">缓存搜索php7<br></p>\n<blockquote style=\"box-sizing: border-box; padding: 5px 20px; margin: 15px 0px; border-width: 1px 1px 1px 8px; border-style: solid; border-color: rgb(221, 221, 221); font-family: \"><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">apt-cache search php7 | grep php7</p></blockquote>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">安装php</p>\n<blockquote style=\"box-sizing: border-box; padding: 5px 20px; margin: 15px 0px; border-width: 1px 1px 1px 8px; border-style: solid; border-color: rgb(221, 221, 221); font-family: \"><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">apt-get install php7.0</p></blockquote>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">完成之后输入</p>\n<blockquote style=\"box-sizing: border-box; padding: 5px 20px; margin: 15px 0px; border-width: 1px 1px 1px 8px; border-style: solid; border-color: rgb(221, 221, 221); font-family: \"><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">php -v</p></blockquote>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">会看到已安装的版本PHP7.0</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">这个时候，Apache运行则还是无法编译php代码的，会出现原文输出的效果，就像文本格式一样，</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">进入  /etc/apache2/sites-available/</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">查找到：</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; white-space: pre;\"></span>SSLOptions +StdEnvVars</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; white-space: pre;\"></span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">修改为：</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; white-space: pre;\"></span>SSLOptions +StdEnvVars</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; white-space: pre;\"></span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">重启apache</p>\n<blockquote style=\"box-sizing: border-box; padding: 5px 20px; margin: 15px 0px; border-width: 1px 1px 1px 8px; border-style: solid; border-color: rgb(221, 221, 221); font-family: \"><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">service apache2 restart</p></blockquote>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">完美解决了！！！</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; font-size: 20px; color: rgb(255, 0, 0);\"><strong style=\"box-sizing: border-box;\">三、安装MYSQL</strong></span></p>\n<blockquote style=\"box-sizing: border-box; padding: 5px 20px; margin: 15px 0px; border-width: 1px 1px 1px 8px; border-style: solid; border-color: rgb(221, 221, 221); font-family: \"><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">apt-get install mysql-server -y</p></blockquote>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">输入自己数据库的密码<br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: \">之后就完成了！</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 16px; font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 16px; font-family: \">数据库安全配置</p>\n<blockquote style=\"box-sizing: border-box; padding: 5px 20px; margin: 15px 0px; border-width: 1px 1px 1px 8px; border-style: solid; border-color: rgb(221, 221, 221); font-family: \"><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">mysql_secure_installation</p></blockquote>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 16px; font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 16px; font-family: \">进入数据库</p>\n<blockquote style=\"box-sizing: border-box; padding: 5px 20px; margin: 15px 0px; border-width: 1px 1px 1px 8px; border-style: solid; border-color: rgb(221, 221, 221); font-family: \">\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">mysql -u root -p \'你的密码\'</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">grant all privileges on *.* to \'root\'@\'%\' identified by \'你的账户密码\' with grant option;---这个主要用户外网连接数据库，如果你不需要的话，就不用设置了</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 14px; line-height: 25px; border: 0px; padding: 5px 0px;\">flush privileges---操作生效<br></p>\n</blockquote>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><br></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px;\">然后添加用户权限---mysql中</span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px;\"><span style=\"box-sizing: border-box; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><span style=\"box-sizing: border-box;\">grant all privileges on *.* to \'<a href=\"mailto:\'root\'@\'%\'\" target=\"_blank\" style=\"box-sizing: border-box; color: rgb(51, 102, 153); text-decoration-line: none;\">root</a>\'@\'<a href=\"mailto:\'root\'@\'%\'\" target=\"_blank\" style=\"box-sizing: border-box; color: rgb(51, 102, 153); text-decoration-line: none;\">%</a>\' identified by \'<span style=\"box-sizing: border-box;\">密码</span>\' with grant option;</span>---------进入数据库的时候已经设置了，这里就不需要再次设置了！</span></span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px;\"><span style=\"box-sizing: border-box; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><br></span></span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px;\"><span style=\"box-sizing: border-box; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\">最后一步：</span></span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px;\"><span style=\"box-sizing: border-box; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"></span></span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; color: rgb(51, 51, 51);\">关于外网访问数据库连接(创建自己的数据库之后再操作，否则可能会影响数据库的命令创建)</span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px;\">进入 /etc/mysql/my.conf  </span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px;\">查找bind-address的配置， 修改前为：bind-address = 127.0.0.1</span></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \"><span style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px;\">修改后为：<span style=\"box-sizing: border-box; font-size: 13.3333px;\">bind-address = 0.0.0.0</span></span></p>\n<p><br></p>');
INSERT INTO `cnpscy_article_contents` VALUES (159, '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">我不喜欢背锅，不喜欢写前端，哪怕只是一些jq效果，那不是我的事情，不要把这些当做理所应当，工作是工作，我告诉你了应该怎么写？然后你为什么不去百度尝试，而是一句我不会，然后就这样了？呵呵，特么还是我一个后端主动找你，教你怎么写？到底是谁学谁教？ok，我教你怎么写，我边写边给你解释，你呢，你在干嘛？你不听？然后其他一些效果的反馈问题，你说这是我的问题，呵呵，我说这是前端谁的问题，说我的，我特么是PHP，我是后端，那我说:我的锅，ok？我已经不想说话了，说什么。反正前端后台有问题，都是我的该？天大的笑话，人生若是如此，那么有何意义？私下关系是私下关系，工作是工作，我认真严肃的告诉你，你却如此笑话，ok，我前端标签写错了，很好笑吗？那你来写啊？人生无语。我不喜欢解释，解释对我而言没有意义，为何不去解决问题？程序有问题，本身就是程序员的问题。后端我的锅我自己接，前端也是我接？对接接口原本其实有时候也是前端需要做的，现在只要你写好前端，其余都是我来解决，前端不理想就算了，还给我锅，哈哈？不知道大城市是如何的，前后端彻底分离，后端只用提供接口就好了，这样不是更好吗？19年去深圳吧，不考虑任何其他因素而做停留，趁着年轻就早点出去看看世界，不看看世界有多大，如何快速进步呢？不知为何，你可以如此理直气壮的说帮我改了很多BUG，我说我的问题？你说是啊。我就呵呵笑了。样式是前端的问题还是后端的问题？我是后端，还是我的问题？</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">2.6日，相对于今天的昨天，说的很清楚，他说帮我改了很多BUG，我当时就笑了，你什么时候修改过php，MD？我说:样式是我的问题？他说:是啊。我心里万马奔腾，卧槽，MMP的。我真的不想和他扯那么多，真他妈的烦，多少次了，我特么的都数不过来了，一次又一次，蹬鼻子上脸？我靠，一直总是说是我的问题。然后下午说那个修改的我不上传，我特么的老子跟你说了至少三遍，同事在用，晚点上传，你倒好，说我不想上传，我真特么的不想解释。我说:对，我就是不想上传，咋了，传了又怎样。ok，我就忙去了，呵呵哒。</p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(57, 57, 57); font-family: \">你自己觉得你很牛逼还是怎样？有那个程序员把什么锅都抛给后端的？特么的你有自己的主见，有自己的原则，有自己的责任吗？！要不然你以后后端也归你啊，你来写啊？你不是牛逼的帮我改BUG吗？以后jq效果你自己写啊，别找我啊？</p>\n<p><br></p>');
INSERT INTO `cnpscy_article_contents` VALUES (160, '<pre class=\"content\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; white-space: pre-wrap; font-family: punctuation, \" microsoft yahei display: inline>修改了layui的table模块\r\n只能说这个js会限制接口的格式，如果接口格式不按照官网的模式走，全部都会蹦掉。\r\n改好了已经，现在就随意接口了，指定一下就好了的</pre>\n<p><a class=\"has_more_con \" style=\"color: rgb(122, 166, 33);\"></a></p>\n<p><a href=\"http://b308.photo.store.qq.com/psb?/V12RLLPk0vHKo2/*eYDRPmp1qKoxSIzmV3ntFm4YKmbzrcCwjL4QqfxFCc!/c/dDQBAAAAAAAA&amp;bo=AAX3AgAF9wIBACc!\" title=\"查看大图\" style=\"color: rgb(122, 166, 33); text-decoration-line: none; width: 196px; height: 196px; float: left; margin: 0px 8px 8px 0px; overflow: hidden; position: relative; text-align: center;\"><img src=\"http://b308.photo.store.qq.com/psb?/V12RLLPk0vHKo2/*eYDRPmp1qKoxSIzmV3ntFm4YKmbzrcCwjL4QqfxFCc!/c/dDQBAAAAAAAA&amp;bo=AAX3AgAF9wIBACc!&amp;rf=mood_app&amp;t=5\" class=\"\" height=\"200\"></a><a href=\"http://b310.photo.store.qq.com/psb?/V12RLLPk0vHKo2/1YRyYnXH63dI0A3eHdRZaUq77G1pBlGXL2sTGmfl2*0!/c/dDYBAAAAAAAA&amp;bo=xgIjAcYCIwEBACc!\" title=\"查看大图\" style=\"color: rgb(122, 166, 33); text-decoration-line: none; width: 196px; height: 196px; float: left; margin: 0px 8px 8px 0px; overflow: hidden; position: relative; text-align: center;\"><img src=\"http://b310.photo.store.qq.com/psb?/V12RLLPk0vHKo2/1YRyYnXH63dI0A3eHdRZaUq77G1pBlGXL2sTGmfl2*0!/c/dDYBAAAAAAAA&amp;bo=xgIjAcYCIwEBACc!&amp;rf=mood_app&amp;t=5\" class=\"\" height=\"200\"></a><a href=\"http://b310.photo.store.qq.com/psb?/V12RLLPk0vHKo2/338rrtkE1SaxAlXSYEmBD11XeJezqYgmc.G1T2KuirE!/c/dDYBAAAAAAAA&amp;bo=9gJMAvYCTAIBACc!\" title=\"查看大图\" style=\"color: rgb(122, 166, 33); text-decoration-line: none; width: 196px; height: 196px; float: left; margin: 0px 8px 8px 0px; overflow: hidden; position: relative; text-align: center;\"><img src=\"http://b310.photo.store.qq.com/psb?/V12RLLPk0vHKo2/338rrtkE1SaxAlXSYEmBD11XeJezqYgmc.G1T2KuirE!/c/dDYBAAAAAAAA&amp;bo=9gJMAvYCTAIBACc!&amp;rf=mood_app&amp;t=5\" class=\"\" height=\"200\"></a><a href=\"http://b303.photo.store.qq.com/psb?/V12RLLPk0vHKo2/Kb36sDn2kdcWSL7OYlvk30SB6CgXaeTTqKsUsJChLNw!/c/dC8BAAAAAAAA&amp;bo=BQPNAQUDzQEBACc!\" title=\"查看大图\" style=\"color: rgb(122, 166, 33); width: 196px; height: 196px; float: left; margin: 0px 8px 8px 0px; overflow: hidden; position: relative; text-align: center;\"><img src=\"http://b303.photo.store.qq.com/psb?/V12RLLPk0vHKo2/Kb36sDn2kdcWSL7OYlvk30SB6CgXaeTTqKsUsJChLNw!/c/dC8BAAAAAAAA&amp;bo=BQPNAQUDzQEBACc!&amp;rf=mood_app&amp;t=5\" class=\"\" height=\"200\"></a></p>\n<p><br></p>');
INSERT INTO `cnpscy_article_contents` VALUES (161, '<p>已经解决了</p>\n<p><img src=\"https://blog.cnpscy.cn/assets/ueditor1_4_3_3-utf8-php/themes/default/images/spacer.gif\">官网的图：<img src=\"/storage/_ueditor/image/20190929/1569749752953827.png\" title=\"1569749752953827.png\" alt=\"QQ图片20190929173529.png\"></p>');

-- ----------------------------
-- Table structure for cnpscy_article_datas
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_article_datas`;
CREATE TABLE `cnpscy_article_datas`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章主键',
  `article_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章内容',
  `create_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `browser_type` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建时浏览器类型',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `article_id`(`article_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章内容表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_article_datas
-- ----------------------------
INSERT INTO `cnpscy_article_datas` VALUES (1, 1, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (2, 2, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (3, 3, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (4, 4, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (5, 5, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (6, 6, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (7, 7, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (8, 8, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (9, 9, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (10, 10, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (11, 11, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (12, 12, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (13, 13, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (14, 14, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (15, 15, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (16, 16, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (17, 17, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (18, 18, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (19, 19, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (20, 20, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (21, 21, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (22, 22, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (23, 23, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (24, 24, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (25, 25, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (26, 26, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (27, 27, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (28, 28, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (29, 29, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (30, 30, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (31, 31, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (32, 32, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (33, 33, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (34, 34, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (35, 35, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (36, 36, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (37, 37, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (38, 38, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (39, 39, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (40, 40, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (41, 41, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (42, 42, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (43, 43, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (44, 44, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (45, 45, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (46, 46, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (47, 47, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (48, 48, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (49, 49, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (50, 50, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (51, 51, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (52, 52, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (53, 53, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (54, 54, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (55, 55, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (56, 56, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (57, 57, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (58, 58, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (59, 59, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (60, 60, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (61, 61, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (62, 62, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (63, 63, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (64, 64, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (65, 65, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (66, 66, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (67, 67, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (68, 68, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (69, 69, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (70, 70, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (71, 71, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (72, 72, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (73, 73, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (74, 74, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (75, 75, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (76, 76, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (77, 77, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (78, 78, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (79, 79, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (80, 80, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (81, 81, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (82, 82, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (83, 83, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (84, 84, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (85, 85, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (86, 86, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (87, 87, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (88, 88, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (89, 89, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (90, 90, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (91, 91, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (92, 92, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (93, 93, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (94, 94, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (95, 95, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (96, 96, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (97, 97, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (98, 98, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (99, 99, '', '', '');
INSERT INTO `cnpscy_article_datas` VALUES (100, 100, '&lt;p&gt;1113大事&lt;/p&gt;&lt;p&gt;发生大法&lt;/p&gt;&lt;p&gt;师法防守打法&lt;/p&gt;&lt;p&gt;发多少&lt;/p&gt;', '', '');

-- ----------------------------
-- Table structure for cnpscy_article_labels
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_article_labels`;
CREATE TABLE `cnpscy_article_labels`  (
  `label_id` int(11) NOT NULL AUTO_INCREMENT,
  `label_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标签名',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`label_id`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE,
  INDEX `create_time`(`created_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章标签表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_article_labels
-- ----------------------------
INSERT INTO `cnpscy_article_labels` VALUES (1, '1', 0, 1610331482, 1610331482);
INSERT INTO `cnpscy_article_labels` VALUES (2, '2', 0, 1610331485, 1610331485);
INSERT INTO `cnpscy_article_labels` VALUES (3, '4', 0, 1610331490, 1610331498);
INSERT INTO `cnpscy_article_labels` VALUES (4, '3', 0, 1610331493, 1610331493);
INSERT INTO `cnpscy_article_labels` VALUES (5, '5', 0, 1610331501, 1610331501);

-- ----------------------------
-- Table structure for cnpscy_article_with_labels
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_article_with_labels`;
CREATE TABLE `cnpscy_article_with_labels`  (
  `with_id` int(11) NOT NULL AUTO_INCREMENT,
  `label_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章标签Id',
  `article_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章Id',
  PRIMARY KEY (`with_id`) USING BTREE,
  INDEX `label_id`(`label_id`) USING BTREE,
  INDEX `article_id`(`article_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章标签关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_article_with_labels
-- ----------------------------
INSERT INTO `cnpscy_article_with_labels` VALUES (37, 2, 168);
INSERT INTO `cnpscy_article_with_labels` VALUES (38, 4, 168);

-- ----------------------------
-- Table structure for cnpscy_articles
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_articles`;
CREATE TABLE `cnpscy_articles`  (
  `article_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文章表',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员Id',
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类Id',
  `article_title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文章标题',
  `article_images` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '封面',
  `article_keywords` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '关键词',
  `article_description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述',
  `article_sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `set_top` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '置顶',
  `is_recommend` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '推荐',
  `is_public` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否公开：0：私密；1：是；2.密码访问',
  `access_password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '访问密码',
  `article_origin` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文章来源',
  `article_author` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文章作者',
  `article_link` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '详情外链',
  `read_num` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读数量',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '创建时的IP',
  `praise_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点赞数量',
  `collection_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '收藏数量',
  `comment_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论数量',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `article_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '内容',
  PRIMARY KEY (`article_id`) USING BTREE,
  INDEX `articles_user_id_index`(`user_id`) USING BTREE,
  INDEX `articles_is_public_index`(`is_public`) USING BTREE,
  INDEX `articles_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 169 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_articles
-- ----------------------------
INSERT INTO `cnpscy_articles` VALUES (123, 0, 0, '最近一直听的歌曲~邓紫棋-光年之外', '', '最近一直听的歌曲~邓紫棋-光年之外', '最近一直听的歌曲~邓紫棋-光年之外', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/27', '小丑路人', '', 167, 0, '', 0, 0, 0, 1601226589, 1601226589, NULL);
INSERT INTO `cnpscy_articles` VALUES (124, 0, 0, 'Mysql读写分离，主从同步配置成功', '', 'Mysql读写分离，主从同步配置成功', 'Mysql读写分离，主从同步配置成功', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/32', '小丑路人', '', 134, 0, '', 0, 0, 0, 1601226589, 1601226589, NULL);
INSERT INTO `cnpscy_articles` VALUES (125, 0, 0, '关于lumen，模型开启 timestamps （时间戳维护-自动更新），并且同时使用 自定义的时间戳字段，在模型中查询，时间戳会被 \\Illuminate\\Support\\Carbon 格式化为正常时间格式', '', '关于lumen，模型开启 timestamps （时间戳维护-自动更新），并且同时使用 自定义的时间戳字段，在模型中查询，时间戳会被 \\Illuminate\\Support\\Carbon 格式化为正常时间格式', '关于lumen，模型开启 timestamps （时间戳维护-自动更新），并且同时使用 自定义的时间戳字段，在模型中查询，时间戳会被 \\Illuminate\\Support\\Carbon 格式化为正常时间格式', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/35', '小丑路人', '', 382, 0, '', 0, 0, 0, 1601226589, 1601226589, NULL);
INSERT INTO `cnpscy_articles` VALUES (126, 0, 0, '商场【shop.cnpscy.cn】', '', '商场【shop.cnpscy.cn】', '商场【shop.cnpscy.cn】', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/36', '小丑路人', '', 133, 0, '', 0, 0, 0, 1601226589, 1601226589, NULL);
INSERT INTO `cnpscy_articles` VALUES (127, 0, 0, '新闻资讯 - 混合APP', '', '新闻资讯 - 混合APP', '新闻资讯 - 混合APP', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/37', '小丑路人', '', 146, 0, '', 0, 0, 0, 1601226589, 1601226589, NULL);
INSERT INTO `cnpscy_articles` VALUES (128, 0, 0, 'swoole安装编译', '', 'swoole安装编译', 'swoole安装编译', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/39', '小丑路人', '', 153, 0, '', 0, 0, 0, 1601226589, 1601226589, NULL);
INSERT INTO `cnpscy_articles` VALUES (129, 0, 0, 'laravel基础版后台公开', '', 'laravel基础版后台公开', 'laravel基础版后台公开', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/40', '小丑路人', '', 153, 0, '', 0, 0, 0, 1601226589, 1601226589, NULL);
INSERT INTO `cnpscy_articles` VALUES (130, 0, 0, '仅此只仅仅针对于layuicms2.0，适用于laravel框架时，iframe子页面内容存在，但是数据隐藏', '', '仅此只仅仅针对于layuicms2.0，适用于laravel框架时，iframe子页面内容存在，但是数据隐藏', '仅此只仅仅针对于layuicms2.0，适用于laravel框架时，iframe子页面内容存在，但是数据隐藏', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/31', '小丑路人', '', 136, 0, '', 0, 0, 0, 1601226589, 1601226589, NULL);
INSERT INTO `cnpscy_articles` VALUES (131, 0, 0, '关于Lumen开发，软连接的生成', '', '关于Lumen开发，软连接的生成', '关于Lumen开发，软连接的生成', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/34', '小丑路人', '', 254, 0, '', 0, 0, 0, 1601226589, 1601226589, NULL);
INSERT INTO `cnpscy_articles` VALUES (132, 0, 0, '无题（一）', '', '无题（一）', '无题（一）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/1', '小丑路人', '', 101, 0, '', 0, 0, 0, 1601226589, 1601226589, NULL);
INSERT INTO `cnpscy_articles` VALUES (133, 0, 0, '无题（二）', '', '无题（二）', '无题（二）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/2', '小丑路人', '', 73, 0, '', 0, 0, 0, 1601226592, 1601226592, NULL);
INSERT INTO `cnpscy_articles` VALUES (134, 0, 0, '无题（三）', '', '无题（三）', '无题（三）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/3', '小丑路人', '', 67, 0, '', 0, 0, 0, 1601226592, 1601226592, NULL);
INSERT INTO `cnpscy_articles` VALUES (135, 0, 0, '无题（四）', '', '无题（四）', '无题（四）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/4', '小丑路人', '', 75, 0, '', 0, 0, 0, 1601226592, 1601226592, NULL);
INSERT INTO `cnpscy_articles` VALUES (136, 0, 0, '无题（五）', '', '无题（五）', '无题（五）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/5', '小丑路人', '', 77, 0, '', 0, 0, 0, 1601226592, 1601226592, NULL);
INSERT INTO `cnpscy_articles` VALUES (137, 0, 0, '无题（六）', '', '无题（六）', '无题（六）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/6', '小丑路人', '', 77, 0, '', 0, 0, 0, 1601226592, 1601226592, NULL);
INSERT INTO `cnpscy_articles` VALUES (138, 0, 0, '无题（七）', '', '无题（七）', '无题（七）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/7', '小丑路人', '', 74, 0, '', 0, 0, 0, 1601226592, 1601226592, NULL);
INSERT INTO `cnpscy_articles` VALUES (139, 0, 0, '无题（八）', '', '无题（八）', '无题（八）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/8', '小丑路人', '', 72, 0, '', 0, 0, 0, 1601226592, 1601226592, NULL);
INSERT INTO `cnpscy_articles` VALUES (140, 0, 0, '无题（九）', '', '无题（九）', '无题（九）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/9', '小丑路人', '', 71, 0, '', 0, 0, 0, 1601226592, 1601226592, NULL);
INSERT INTO `cnpscy_articles` VALUES (141, 0, 0, '无题（十）', '', '无题（十）', '无题（十）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/10', '小丑路人', '', 69, 0, '', 0, 0, 0, 1601226592, 1601226592, NULL);
INSERT INTO `cnpscy_articles` VALUES (142, 0, 0, '无题（十一）', '', '无题（十一）', '无题（十一）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/11', '小丑路人', '', 78, 0, '', 0, 0, 0, 1601226592, 1601226592, NULL);
INSERT INTO `cnpscy_articles` VALUES (143, 0, 0, '无题（十二）', '', '无题（十二）', '无题（十二）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/12', '小丑路人', '', 81, 0, '', 0, 0, 0, 1601226594, 1601226594, NULL);
INSERT INTO `cnpscy_articles` VALUES (144, 0, 0, '无题（十三）', '', '无题（十三）', '无题（十三）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/13', '小丑路人', '', 76, 0, '', 0, 0, 0, 1601226594, 1601226594, NULL);
INSERT INTO `cnpscy_articles` VALUES (145, 0, 0, '挥手一笔', '', '挥手一笔', '挥手一笔', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/14', '小丑路人', '', 121, 0, '', 0, 0, 0, 1601226594, 1601226594, NULL);
INSERT INTO `cnpscy_articles` VALUES (146, 0, 0, '无题（十四）', '', '无题（十四）', '无题（十四）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/15', '小丑路人', '', 66, 0, '', 0, 0, 0, 1601226594, 1601226594, NULL);
INSERT INTO `cnpscy_articles` VALUES (147, 0, 0, '车行遇（一）', '', '车行遇（一）', '车行遇（一）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/16', '小丑路人', '', 75, 0, '', 0, 0, 0, 1601226594, 1601226594, NULL);
INSERT INTO `cnpscy_articles` VALUES (148, 0, 0, '车行遇（二）', '', '车行遇（二）', '车行遇（二）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/17', '小丑路人', '', 81, 0, '', 0, 0, 0, 1601226594, 1601226594, NULL);
INSERT INTO `cnpscy_articles` VALUES (149, 0, 0, '无题（十五）', '', '无题（十五）', '无题（十五）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/18', '小丑路人', '', 119, 0, '', 0, 0, 0, 1601226594, 1601226594, NULL);
INSERT INTO `cnpscy_articles` VALUES (150, 0, 0, '无题（十六）', '', '无题（十六）', '无题（十六）', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/19', '小丑路人', '', 69, 0, '', 0, 0, 0, 1601226594, 1601226594, NULL);
INSERT INTO `cnpscy_articles` VALUES (151, 0, 0, 'mysql操作语句', '', 'mysql操作语句', 'mysql操作语句', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/20', '小丑路人', '', 75, 0, '', 0, 0, 0, 1601226594, 1601226594, NULL);
INSERT INTO `cnpscy_articles` VALUES (152, 0, 0, 'Mysql --- group_concat', '', 'Mysql --- group_concat', 'Mysql --- group_concat', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/21', '小丑路人', '', 65, 0, '', 0, 0, 0, 1601226594, 1601226594, NULL);
INSERT INTO `cnpscy_articles` VALUES (153, 0, 0, '梦境迷糊不定，深不可测', '', '梦境迷糊不定，深不可测', '梦境迷糊不定，深不可测', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/22', '小丑路人', '', 87, 0, '', 0, 0, 0, 1601226597, 1601226597, NULL);
INSERT INTO `cnpscy_articles` VALUES (154, 0, 0, '直播技术点', '', '直播技术点', '直播技术点', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/23', '小丑路人', '', 81, 0, '', 0, 0, 0, 1601226597, 1601226597, NULL);
INSERT INTO `cnpscy_articles` VALUES (155, 0, 0, '人枉然！', '', '人枉然！', '人枉然！', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/24', '小丑路人', '', 78, 0, '', 0, 0, 0, 1601226597, 1601226597, NULL);
INSERT INTO `cnpscy_articles` VALUES (156, 0, 0, '不断加油', '', '不断加油', '不断加油', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/25', '小丑路人', '', 102, 0, '', 0, 0, 0, 1601226597, 1601226597, NULL);
INSERT INTO `cnpscy_articles` VALUES (157, 0, 0, '终于离职成功！好可怕的世界', '', '终于离职成功！好可怕的世界', '终于离职成功！好可怕的世界', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/26', '小丑路人', '', 86, 0, '', 0, 0, 0, 1601226597, 1601226597, NULL);
INSERT INTO `cnpscy_articles` VALUES (158, 0, 0, 'Debian8 安装PHP7.0+Apache2+Mysql5.5', '', 'Debian8 安装PHP7.0+Apache2+Mysql5.5', 'Debian8 安装PHP7.0+Apache2+Mysql5.5', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/29', '小丑路人', '', 144, 0, '', 0, 0, 0, 1601226597, 1601226597, NULL);
INSERT INTO `cnpscy_articles` VALUES (159, 0, 0, '感觉这样的人生有何意义？', '', '感觉这样的人生有何意义？', '感觉这样的人生有何意义？', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/30', '小丑路人', '', 96, 0, '', 0, 0, 0, 1601226597, 1601226597, NULL);
INSERT INTO `cnpscy_articles` VALUES (160, 0, 0, 'Layui-table', '', 'Layui-table', 'Layui-table', 0, 0, 0, 1, '', 'https://blog.cnpscy.cn/article-detail/33', '小丑路人', '', 100, 0, '', 0, 0, 0, 1601226597, 1601226597, NULL);
INSERT INTO `cnpscy_articles` VALUES (161, 0, 0, '关于editor.md，跨域图片上传---已经解决了', '', '关于editor.md，跨域图片上传---已经解决了', '关于editor.md，跨域图片上传---已经解决了', 0, 1, 1, 1, '', 'https://blog.cnpscy.cn/article-detail/38', '小丑路人', '', 129, 0, '', 0, 0, 0, 1601226597, 1609901624, NULL);
INSERT INTO `cnpscy_articles` VALUES (162, 0, 0, '111', '202105/LM6jSgpx3zhBwOlQzCpYPMKCMwBGhbVhjaM2uBRm.jpg', '', '', 99, 1, 0, 0, '', '', '', '', 0, 0, '', 0, 0, 0, 1609393184, 1609901628, NULL);
INSERT INTO `cnpscy_articles` VALUES (163, 0, 21, '111222', '202105/LM6jSgpx3zhBwOlQzCpYPMKCMwBGhbVhjaM2uBRm.jpg', '123123', '3124324242432', 1, 0, 0, 1, '', '', '', '', 0, 1, '', 0, 0, 0, 1609393197, 1609825673, '的撒发沙发撒旦法撒旦法安抚');
INSERT INTO `cnpscy_articles` VALUES (164, 0, 95, '111111111111', '202105/LM6jSgpx3zhBwOlQzCpYPMKCMwBGhbVhjaM2uBRm.jpg', '2132', '131', 99, 0, 0, 1, '', '', '', '', 0, 1, '', 0, 0, 0, 1609825471, 1609825649, '请求');
INSERT INTO `cnpscy_articles` VALUES (165, 0, 95, '123123', '202105/LM6jSgpx3zhBwOlQzCpYPMKCMwBGhbVhjaM2uBRm.jpg', '', '', 99, 0, 0, 0, '', '', '', '', 0, 0, '', 0, 0, 0, 1610331707, 1610331707, '24234');
INSERT INTO `cnpscy_articles` VALUES (168, 0, 15, '1231233213213', '202105/54AD4BLJ6iEqq4DxXlh6u0eSuWOgotG62nHKvTrW.jpg,202105/YVjXcMSLdmMPtuYIQtY5SRLL5I00ZwLyn3WmaTAO.jpg,202105/pcZfhpUmDNauCm1CZBiz8jurcXwiunDVI8yxwJwr.jpg', '', '', 99, 0, 0, 0, '', '', '', '', 0, 0, '', 0, 0, 0, 1610332409, 1621673881, '24234');

-- ----------------------------
-- Table structure for cnpscy_banners
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_banners`;
CREATE TABLE `cnpscy_banners`  (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `banner_type` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Banner类型：common_banner_type_list',
  `banner_cover` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '封面',
  `banner_link` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '外链',
  `banner_words` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文字描述',
  `banner_sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序[从小到大]',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `is_check` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否可用：1：可用；0：禁用',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`banner_id`) USING BTREE,
  INDEX `banner_type`(`banner_type`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE,
  INDEX `is_check`(`is_check`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'banner表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_banners
-- ----------------------------
INSERT INTO `cnpscy_banners` VALUES (1, '111', 0, '202101/RjF3oncem8soRMDAAknZBi560OrM4wk2oCa3SRv3.png', 'http://www.cnpscy.cn', '', 1, 0, 1, 1586485719, 1610030465);
INSERT INTO `cnpscy_banners` VALUES (2, '131221', 0, '202101/yt5BnHVc9AGTCFYR7rOCRsdI3hKH5c9qTx059Fvq.png', NULL, '', 0, 1, 1, 1609826532, 1609827378);

-- ----------------------------
-- Table structure for cnpscy_configs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_configs`;
CREATE TABLE `cnpscy_configs`  (
  `config_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '网站配置信息表',
  `config_title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `config_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '参数名称',
  `config_value` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '参数值',
  `config_group` smallint(6) NOT NULL DEFAULT 0 COMMENT '分组',
  `config_extra` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '配置项',
  `config_type` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '类型：0.字符串；1.数字；2.文本；3.select下拉框；4.图片；5.富文本',
  `config_sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `config_remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '说明',
  `is_check` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否审核/可用',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`config_id`) USING BTREE,
  INDEX `configs_created_time_index`(`created_time`) USING BTREE,
  INDEX `configs_config_type_index`(`config_type`) USING BTREE,
  INDEX `configs_is_check_index`(`is_check`) USING BTREE,
  INDEX `configs_config_sort_index`(`config_sort`) USING BTREE,
  INDEX `configs_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 63 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_configs
-- ----------------------------
INSERT INTO `cnpscy_configs` VALUES (1, '网站标题', 'site_web_title', '小丑路人博客', 1, '', 1, 1, '', 1, 1542609152, 1542609573, 0);
INSERT INTO `cnpscy_configs` VALUES (2, '网站作者', 'site_web_author', '小丑路人', 1, '', 1, 2, '', 1, 1542610198, 1542610198, 0);
INSERT INTO `cnpscy_configs` VALUES (3, '网站关键字搜索', 'site_web_keywords', '小丑路人,小丑,路人,小丑疯狂吧,小丑博客,博客,laravel,yii,mysql', 1, '', 1, 3, '', 1, 1542610270, 1542610270, 0);
INSERT INTO `cnpscy_configs` VALUES (4, '网站描述', 'site_web_description', '小丑路人,小丑,路人,小丑疯狂吧,小丑博客,博客,laravel,yii,mysql', 1, '', 1, 4, '', 1, 1542610411, 1542610411, 0);
INSERT INTO `cnpscy_configs` VALUES (5, '网站备案号', 'site_web_site_icp', '晋ICP备18007574号-1', 1, '', 1, 5, '', 1, 1542610443, 1542610443, 0);
INSERT INTO `cnpscy_configs` VALUES (6, '网站Logo', 'site_web_logo', '/static/blog.jpg', 1, '', 1, 6, '', 1, 1542610488, 1542610488, 0);
INSERT INTO `cnpscy_configs` VALUES (7, '是否开启接口日志', 'START_API_LOGS', '1', 4, '0:不开启\r\n1:开启', 4, 7, '', 1, 1542610547, 1542610547, 0);
INSERT INTO `cnpscy_configs` VALUES (8, '文章缓存时长', 'ARTICLE_CACHE_TIME', '60', 4, '', 4, 8, '前台文章详情数据\r\n文章缓存时长（s）', 1, 1542610592, 1542610592, 0);
INSERT INTO `cnpscy_configs` VALUES (9, '站点是否关闭', 'WEB_SITE_CLOSE', '1', 1, '0:关闭\r\n1:开启', 1, 9, '', 1, 1542610636, 1542610636, 0);
INSERT INTO `cnpscy_configs` VALUES (10, '配置分组', 'config_group_list', '0:不分组\r\n1:基本\r\n2:内容\r\n3:用户\r\n4:系统\r\n5:博主\r\n6:开发者\r\n7:网站后台\r\n8:JWT\r\n9:小丑社区配置\r\n10:数据库', 4, '', 3, 10, '', 1, 1542610809, 1542617389, 0);
INSERT INTO `cnpscy_configs` VALUES (11, '配置类型列表', 'config_type_list', '0:字符串\r\n1:文本\r\n2:数字\r\n3:数组\r\n4:枚举\r\n5:图片', 4, '', 3, 11, '', 1, 1542610869, 1542610869, 0);
INSERT INTO `cnpscy_configs` VALUES (12, '全站默认展示图', 'web_detault_show_img', '/static/blog.jpg', 1, '', 4, 12, '如果图片不存在，默认展示为该图片', 1, 1542610960, 1542610960, 0);
INSERT INTO `cnpscy_configs` VALUES (13, '网站站点URL', 'web_url', '', 1, '', 1, 0, '', 1, 1542611040, 1609825861, 0);
INSERT INTO `cnpscy_configs` VALUES (18, '后台接口前缀', 'api_admin_prefix', 'admin', 6, '', 0, 17, '', 1, 1542616391, 1542616391, 0);
INSERT INTO `cnpscy_configs` VALUES (27, '网站后台的TITLE', 'web_admin_title', '小丑路人博客后台管理', 4, '', 0, 26, '', 1, 1542616660, 1542616660, 0);
INSERT INTO `cnpscy_configs` VALUES (28, '网站后台的关键字', 'web_admin_keywords', '小丑路人博客后台', 4, '', 0, 27, '', 1, 1542616679, 1542616679, 0);
INSERT INTO `cnpscy_configs` VALUES (29, '网站后台描述', 'web_admin_description', '小丑路人博客后台', 4, '', 0, 28, '', 1, 1542616718, 1542616718, 0);
INSERT INTO `cnpscy_configs` VALUES (30, '前端菜单类型列表', 'menu_type_list', '0:作为频道页，不可作为栏目发布文章\r\n1:不直接发布内容，用于跳转页面\r\n2:作为发布栏目，文章列表模式\r\n3:单页面模式，例如企业简介', 2, '', 3, 29, '', 1, 1542616835, 1542616835, 0);
INSERT INTO `cnpscy_configs` VALUES (31, '网站后台访问路径前缀', 'admin_prefix', 'admin', 4, '', 0, 30, '', 1, 1542616866, 1542616866, 0);
INSERT INTO `cnpscy_configs` VALUES (33, '后台视图文件夹名称', 'ADMIN_VIEWS', '/_admin/', 6, '', 0, 32, '', 1, 1542616912, 1542616912, 0);
INSERT INTO `cnpscy_configs` VALUES (34, 'JWT-私有Key', 'privateKey', '&lt;&lt;&lt;EOD-----BEGIN RSA PRIVATE KEY-----MIICXAIBAAKBgQC8kGa1pSjbSYZVebtTRBLxBz5H4i2p/llLCrEeQhta5kaQu/RnvuER4W8oDH3+3iuIYW4VQAzyqFpwuzjkDI+17t5t0tyazyZ8JXw+KgXTxldMPEL95+qVhgXvwtihXC1c5oGbRlEDvDF6Sa53rcFVsYJ4ehde/zUxo6UvS7UrBQIDAQABAoGAb/MXV46XxCFRx', 8, '', 8, 0, '', 1, 1542616912, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (35, 'JWT-公钥Key', 'publicKey', '&lt;&lt;&lt;EOD-----BEGIN PUBLIC KEY-----MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8kGa1pSjbSYZVebtTRBLxBz5H4i2p/llLCrEeQhta5kaQu/RnvuER4W8oDH3+3iuIYW4VQAzyqFpwuzjkDI+17t5t0tyazyZ8JXw+KgXTxldMPEL95+qVhgXvwtihXC1c5oGbRlEDvDF6Sa53rcFVsYJ4ehde/zUxo6UvS7UrBQIDAQ', 8, '', 8, 0, '', 1, 4294967295, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (36, 'JWT-token存活时间(s)', 'jwt_leeway', '60', 8, '', 8, 0, '', 1, 1542616912, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (37, '服务器接收token的名称【客户端传递】', 'jwt_token_name', 'cnpscy-token', 9, '', 9, 0, '', 1, 1542616912, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (38, '服务器下发token的名称【服务端下发】', 'return_jwt_token_name', 'jwt_token', 9, '', 9, 0, '', 1, 1542616912, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (39, '服务端的会员标识', 'server_user_unqiue', 'login-user-id', 9, '', 9, 0, '', 1, 1542616912, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (43, '是否开启API日志', 'start_api_logs', '1', 6, '', 6, 0, '', 1, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (44, '是否开启后台操作日志', 'start_admin_operation_logs', '1', 6, '', 6, 0, '', 1, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (45, '网站后台的访问URL', 'admin_prefix_url', 'admin', 0, '', 0, 0, '', 0, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (46, '网站的JS/CSS版本号', 'resource_version_number', '20200410', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (47, '后台-管理员默认密码', 'admin_default_pass', '123456', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (48, 'CDN地址', 'web_cdn_address', '', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (49, '版本号', 'cnpscy_version_number', 'v1.0', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (50, '时区', 'system_timezone', 'Asia/Shanghai', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (51, '禁止访问IP', 'forbiddenip', '', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (52, '模块语言', 'languages', '', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (53, '邮件发送方式', 'mail_type', '0:选择邮件发送方式1:SMTP2:2:mail()函数', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 1);
INSERT INTO `cnpscy_configs` VALUES (54, 'SMTP[服务器]', 'mail_smtp_host', 'smtp.qq.com', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 1);
INSERT INTO `cnpscy_configs` VALUES (55, 'SMTP[端口]', 'mail_smtp_port', '465', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 1);
INSERT INTO `cnpscy_configs` VALUES (56, 'SMTP[用户名]', 'mail_smtp_user', '', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 1);
INSERT INTO `cnpscy_configs` VALUES (57, 'SMTP[密码]', 'mail_smtp_pass', '', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 1);
INSERT INTO `cnpscy_configs` VALUES (58, 'SMTP验证方式', 'mail_verify_type', '0:无1:TLS2:SSL', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 1);
INSERT INTO `cnpscy_configs` VALUES (59, '发件人邮箱', 'mail_from_email', '2278757482@qq.com', 0, '', 0, 0, '', 1, 1586483569, 1586483569, 1);
INSERT INTO `cnpscy_configs` VALUES (60, '权限缓存key', 'admin_menu_session_unique', 'rabc_menus_list', 6, '', 0, 0, '', 1, 1586483569, 1586483569, 0);
INSERT INTO `cnpscy_configs` VALUES (61, '管理员session-key', 'admin_info_session_unique', 'admin_info', 6, '', 0, 0, '', 1, 1586483569, 1609319143, 0);
INSERT INTO `cnpscy_configs` VALUES (62, '管理员权限session-key', 'admin_rabc_session_unique', 'admin_rabc', 6, '', 0, 0, '111', 1, 1586483569, 1609319142, 0);

-- ----------------------------
-- Table structure for cnpscy_failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_failed_jobs`;
CREATE TABLE `cnpscy_failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `cnpscy_failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_friendlinks
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_friendlinks`;
CREATE TABLE `cnpscy_friendlinks`  (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `link_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接URL',
  `link_cover` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接图标',
  `link_sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序[从小到大]',
  `is_check` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否可用：1：可用；0：禁用',
  `open_window` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否打开新窗口：1：是；0：否',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  PRIMARY KEY (`link_id`) USING BTREE,
  INDEX `is_check`(`is_check`) USING BTREE,
  INDEX `link_sort`(`link_sort`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '友情链接表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_friendlinks
-- ----------------------------
INSERT INTO `cnpscy_friendlinks` VALUES (5, 'dsaas', 'javascript:;', '', 0, 1, 0, 1609319170, 1609826175, 1);
INSERT INTO `cnpscy_friendlinks` VALUES (6, 'dasf', 'https://github.com', '202012/YGfkfSZ0aNdczHlXAiXDWmUX4rSl2fGb9eSk2lKf.png', 3, 1, 0, 1609142834, 1609142834, 0);
INSERT INTO `cnpscy_friendlinks` VALUES (7, 'sfds', 'http://laravel-admin.cnpscy.com/cnpscy_blog_admin#/friendlinks', '202105/YVjXcMSLdmMPtuYIQtY5SRLL5I00ZwLyn3WmaTAO.jpg', 0, 0, 0, 1609319170, 1621586781, 0);
INSERT INTO `cnpscy_friendlinks` VALUES (8, 'fg', 'javascript:;', '', 0, 1, 0, 1609319169, 1609901601, 1);
INSERT INTO `cnpscy_friendlinks` VALUES (9, '', 'javascript:;', '', 0, 0, 0, 1609319169, 1609319169, 0);
INSERT INTO `cnpscy_friendlinks` VALUES (10, 'd', 'javascript:;', '', 0, 1, 0, 1609319168, 1609898668, 0);
INSERT INTO `cnpscy_friendlinks` VALUES (11, 'sda', 'javascript:;', '', 0, 0, 0, 1609319168, 1609826227, 1);
INSERT INTO `cnpscy_friendlinks` VALUES (12, 'laravel-admin', 'http://laravel-admin.cnpscy.com/admin#/setting/friendlinks', '202101/KSrOIuf2zWHt0hDBaxd11HkPFQdkK6F66jIcfFv2.png', 1, 1, 0, 1609826212, 1609826223, 0);
INSERT INTO `cnpscy_friendlinks` VALUES (13, '12312321', 'http://laravel-admin.cnpscy.com/cnpscy_blog_admin#/friendlinks', '202105/OVf5dDPc94UHWQXZiqRXVxB0nL5VICMcc9LxGoNs.jpg', 0, 0, 0, 1621586449, 1621586449, 0);

-- ----------------------------
-- Table structure for cnpscy_galleries
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_galleries`;
CREATE TABLE `cnpscy_galleries`  (
  `gallery_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '图库表',
  `gallery_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图库名称',
  `gallery_cover` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '封面',
  `gallery_origin` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '来源链接',
  `total_num` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片总量',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`gallery_id`) USING BTREE,
  INDEX `galleries_is_delete_index`(`is_delete`) USING BTREE,
  INDEX `galleries_created_time_index`(`created_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_gallery_details
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_gallery_details`;
CREATE TABLE `cnpscy_gallery_details`  (
  `detail_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '图库详情表',
  `gallery_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片库Id',
  `gallery_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '文字内容',
  `gallery_pictures` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '图片列表（JSON）',
  `picture_nums` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片数量',
  `origin_link` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '来源链接',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`detail_id`) USING BTREE,
  INDEX `gallery_details_gallery_id_index`(`gallery_id`) USING BTREE,
  INDEX `gallery_details_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_gallery_tags
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_gallery_tags`;
CREATE TABLE `cnpscy_gallery_tags`  (
  `tag_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '图库标签表',
  `tag_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图库标签名称',
  `origin_link` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '来源链接',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`tag_id`) USING BTREE,
  INDEX `gallery_tags_is_delete_index`(`is_delete`) USING BTREE,
  INDEX `gallery_tags_created_time_index`(`created_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_gallery_with_tags
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_gallery_with_tags`;
CREATE TABLE `cnpscy_gallery_with_tags`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '图库与标签关联表',
  `tag_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '标签Id',
  `gallery_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图库Id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gallery_with_tags_tag_id_index`(`tag_id`) USING BTREE,
  INDEX `gallery_with_tags_gallery_id_index`(`gallery_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_good_categories
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_good_categories`;
CREATE TABLE `cnpscy_good_categories`  (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级Id',
  `category_cover` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类封面',
  `category_sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`category_id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_good_categories
-- ----------------------------
INSERT INTO `cnpscy_good_categories` VALUES (1, '服装', 0, '', 0, 0, 1586937049, 1586937049);

-- ----------------------------
-- Table structure for cnpscy_help_centers
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_help_centers`;
CREATE TABLE `cnpscy_help_centers`  (
  `help_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员Id',
  `help_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `help_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`help_id`) USING BTREE,
  INDEX `admin_id`(`admin_id`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '帮助中心表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_help_centers
-- ----------------------------
INSERT INTO `cnpscy_help_centers` VALUES (1, 0, '帮助1', '&lt;p&gt;帮助1帮助1帮助1&lt;/p&gt;&lt;p&gt;帮助1&lt;/p&gt;&lt;p&gt;帮助1&lt;/p&gt;&lt;p&gt;帮助1&lt;/p&gt;&lt;p&gt;帮助1&lt;/p&gt;', 1, 1586923029, 1586923029);
INSERT INTO `cnpscy_help_centers` VALUES (2, 0, '帮助1', '&lt;p&gt;帮助1帮&lt;/p&gt;&lt;p&gt;助1帮助1帮助1帮助1&lt;/p&gt;&lt;p&gt;帮助1&lt;/p&gt;', 0, 1586923042, 1586923673);

-- ----------------------------
-- Table structure for cnpscy_menus
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_menus`;
CREATE TABLE `cnpscy_menus`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级Id',
  `menu_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '栏目名称',
  `menu_tpltype` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '模板类型【0.频道栏目；1.页面跳转；2.列表；3.单页】',
  `menu_listtpl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '列表页模板',
  `menu_detailtpl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '详情模板',
  `menu_img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '单页缩略图',
  `menu_link` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接',
  `menu_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '单页内容内容',
  `menu_keywords` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'head头部展示的关键字搜索',
  `menu_description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'head头部展示的描述',
  `menu_sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序[从小到大]',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：1：删除；0：正常',
  `is_show` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否展示在首页：1：展示；0：隐藏',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`menu_id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE,
  INDEX `is_show`(`is_show`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '前台栏目表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_migrations
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_migrations`;
CREATE TABLE `cnpscy_migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_migrations
-- ----------------------------
INSERT INTO `cnpscy_migrations` VALUES (1, '2020_08_10_182650_create_admins_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (2, '2020_08_10_182726_create_admin_infos_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (3, '2020_08_10_182732_create_admin_logs_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (4, '2020_08_10_182738_create_admin_login_logs_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (6, '2020_08_10_182803_create_admin_roles_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (7, '2020_08_10_182820_create_roles_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (8, '2020_08_10_182831_create_configs_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (9, '2020_08_10_182837_create_banners_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (10, '2020_08_10_182850_create_articles_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (11, '2020_08_10_182859_create_article_categories_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (12, '2020_08_10_182929_create_article_contents_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (13, '2020_08_10_182939_create_friendlinks_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (14, '2020_08_10_182950_create_notices_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (15, '2020_08_10_182959_create_role_menus_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (16, '2020_08_10_182959_create_role_with_menus_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (17, '2020_08_10_183013_create_upload_files_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (18, '2020_08_10_183020_create_upload_groups_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (19, '2020_08_10_183032_create_protocols_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (20, '2020_08_25_151204_create_users_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (21, '2020_08_25_151205_create_user_infos_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (22, '2020_08_25_151447_create_bbs_files_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (23, '2020_08_25_151511_create_bbs_dynamics_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (24, '2020_08_25_151512_create_bbs_dynamic_praises_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (25, '2020_08_25_151523_create_bbs_dynamic_comments_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (26, '2020_08_25_151536_create_bbs_dynamic_collections_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (27, '2020_08_25_151551_create_bbs_settings_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (28, '2020_08_25_151615_create_bbs_chat_records_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (29, '2020_08_25_151630_create_bbs_guestbooks_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (30, '2020_08_25_151650_create_bbs_friends_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (31, '2020_08_25_151652_create_bbs_friend_groups_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (32, '2020_08_25_151705_create_bbs_friend_applies_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (33, '2020_08_25_151719_create_bbs_groups_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (34, '2020_08_25_151729_create_bbs_group_users_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (35, '2020_08_25_151738_create_bbs_group_chats_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (36, '2020_08_25_151748_create_bbs_group_notices_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (37, '2020_08_25_160317_create_bbs_user_settings_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (38, '2020_08_25_161238_create_bbs_albums_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (39, '2020_08_25_161239_create_bbs_album_users_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (40, '2020_08_25_161630_create_adverts_table', 1);
INSERT INTO `cnpscy_migrations` VALUES (41, '2020_09_15_152619_create_notifies', 1);
INSERT INTO `cnpscy_migrations` VALUES (42, '2020_09_15_153803_create_system_notify_users', 1);
INSERT INTO `cnpscy_migrations` VALUES (43, '2020_09_29_105920_create_gallery_tags', 2);
INSERT INTO `cnpscy_migrations` VALUES (44, '2020_09_29_105927_create_galleries', 2);
INSERT INTO `cnpscy_migrations` VALUES (45, '2020_09_29_105938_create_gallery_details', 2);
INSERT INTO `cnpscy_migrations` VALUES (46, '2020_09_29_111311_create_gallery_with_tags', 2);
INSERT INTO `cnpscy_migrations` VALUES (48, '2020_08_10_182755_create_admin_menus_table', 3);
INSERT INTO `cnpscy_migrations` VALUES (49, '2020_11_20_180501_create_bbs_user_signs', 4);
INSERT INTO `cnpscy_migrations` VALUES (50, '2020_11_20_180502_create_bbs_user_grades', 4);
INSERT INTO `cnpscy_migrations` VALUES (51, '2020_11_20_180503_create_bbs_user_experience_records', 4);
INSERT INTO `cnpscy_migrations` VALUES (52, '2020_11_20_180912_create_user_email_verifies', 4);
INSERT INTO `cnpscy_migrations` VALUES (53, '2020_11_20_181117_create_user_login_logs', 4);

-- ----------------------------
-- Table structure for cnpscy_notices
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_notices`;
CREATE TABLE `cnpscy_notices`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_notifies
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_notifies`;
CREATE TABLE `cnpscy_notifies`  (
  `notify_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '系统消息通知记录表',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员Id',
  `is_read` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否已读：1：是；0：否',
  `notify_type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '通知类型：0.系统通知/公告；1.提醒；2.私信',
  `target_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '目标Id(比如动态ID)',
  `target_type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '目标类型：0.动态',
  `sender_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发送者Id',
  `sender_type` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发送者类型：0.系统通知；1.指定会员',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `notify_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '通知内容',
  PRIMARY KEY (`notify_id`) USING BTREE,
  INDEX `notifies_user_id_index`(`user_id`) USING BTREE,
  INDEX `notifies_is_read_index`(`is_read`) USING BTREE,
  INDEX `notifies_notify_type_index`(`notify_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_password_resets`;
CREATE TABLE `cnpscy_password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `cnpscy_password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_protocol_categories
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_protocol_categories`;
CREATE TABLE `cnpscy_protocol_categories`  (
  `category_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '项目分类表',
  `category_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名',
  `category_sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`category_id`) USING BTREE,
  INDEX `blog_protocol_categories_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_protocols
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_protocols`;
CREATE TABLE `cnpscy_protocols`  (
  `protocol_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '协议表',
  `protocol_title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '协议标题',
  `protocol_category` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '协议类型',
  `protocol_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '协议内容',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除（0.未删除；1.已删除）',
  PRIMARY KEY (`protocol_id`) USING BTREE,
  INDEX `blog_protocols_protocol_category_index`(`protocol_category`) USING BTREE,
  INDEX `blog_protocols_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_system_notify_users
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_system_notify_users`;
CREATE TABLE `cnpscy_system_notify_users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '系统消息与会员的已读记录表',
  `notify_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '通知Id',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员Id',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `system_notify_users_user_id_index`(`user_id`) USING BTREE,
  INDEX `system_notify_users_notify_id_index`(`notify_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_table_backups
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_table_backups`;
CREATE TABLE `cnpscy_table_backups`  (
  `backup_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '数据库备份记录表',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '操作人',
  `created_ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '创建IP',
  `tables_name` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备份的表名',
  `file_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小：字节',
  `file_num` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件数量',
  `backup_files` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备份的文件',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：0.否；1.是',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`backup_id`) USING BTREE,
  INDEX `cnpscy_table_backups_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_table_backups
-- ----------------------------
INSERT INTO `cnpscy_table_backups` VALUES (1, 0, '127.0.0.1', 'cnpscy_admin_infos,cnpscy_admin_login_logs,cnpscy_admin_login_logs_2008,cnpscy_admin_logs,cnpscy_admin_menus,cnpscy_admin_menus_copy1,cnpscy_admin_menus_old,cnpscy_admin_role_with_menus,cnpscy_admin_roles,cnpscy_admin_with_roles,cnpscy_admins,cnpscy_adverts,cnpscy_article_categories,cnpscy_article_categorys,cnpscy_article_contents,cnpscy_article_datas,cnpscy_article_labels,cnpscy_article_with_labels,cnpscy_articles,cnpscy_banners,cnpscy_configs,cnpscy_failed_jobs,cnpscy_friendlinks,cnpscy_galleries,cnpscy_gallery_details,cnpscy_gallery_tags,cnpscy_gallery_with_tags,cnpscy_good_categories,cnpscy_help_centers,cnpscy_menus,cnpscy_migrations,cnpscy_notices,cnpscy_notifies,cnpscy_password_resets,cnpscy_protocols,cnpscy_system_notify_users,cnpscy_table_backups,cnpscy_upload_files,cnpscy_upload_groups,cnpscy_users,cnpscy_versions', 5925689, 1, 'D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-27=155748=CtA-1622102268=1.sql', 0, 1622102272, 1622102272);
INSERT INTO `cnpscy_table_backups` VALUES (2, 0, '127.0.0.1', 'cnpscy_admin_infos,cnpscy_admin_login_logs,cnpscy_admin_login_logs_2008,cnpscy_admin_logs,cnpscy_admin_menus,cnpscy_admin_menus_copy1,cnpscy_admin_menus_old,cnpscy_admin_role_with_menus,cnpscy_admin_roles,cnpscy_admin_with_roles,cnpscy_admins,cnpscy_adverts,cnpscy_article_categories,cnpscy_article_categorys,cnpscy_article_contents,cnpscy_article_datas,cnpscy_article_labels,cnpscy_article_with_labels,cnpscy_articles,cnpscy_banners,cnpscy_configs,cnpscy_failed_jobs,cnpscy_friendlinks,cnpscy_galleries,cnpscy_gallery_details,cnpscy_gallery_tags,cnpscy_gallery_with_tags,cnpscy_good_categories,cnpscy_help_centers,cnpscy_menus,cnpscy_migrations,cnpscy_notices,cnpscy_notifies,cnpscy_password_resets,cnpscy_protocols,cnpscy_system_notify_users,cnpscy_table_backups,cnpscy_upload_files,cnpscy_upload_groups,cnpscy_users,cnpscy_versions', 5926899, 1, 'D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-27=155834=5of-1622102314=1.sql', 0, 1622102318, 1622102318);
INSERT INTO `cnpscy_table_backups` VALUES (3, 0, '127.0.0.1', 'cnpscy_admin_infos,cnpscy_admin_login_logs,cnpscy_admin_login_logs_2008,cnpscy_admin_logs,cnpscy_admin_menus,cnpscy_admin_menus_copy1,cnpscy_admin_menus_old,cnpscy_admin_role_with_menus,cnpscy_admin_roles,cnpscy_admin_with_roles,cnpscy_admins,cnpscy_adverts,cnpscy_article_categories,cnpscy_article_categorys,cnpscy_article_contents,cnpscy_article_datas,cnpscy_article_labels,cnpscy_article_with_labels,cnpscy_articles,cnpscy_banners,cnpscy_configs,cnpscy_failed_jobs,cnpscy_friendlinks,cnpscy_galleries,cnpscy_gallery_details,cnpscy_gallery_tags,cnpscy_gallery_with_tags,cnpscy_good_categories,cnpscy_help_centers,cnpscy_menus,cnpscy_migrations,cnpscy_notices,cnpscy_notifies,cnpscy_password_resets,cnpscy_protocols,cnpscy_system_notify_users,cnpscy_table_backups,cnpscy_upload_files,cnpscy_upload_groups,cnpscy_users,cnpscy_versions', 5930090, 6, 'D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=092257=aCP-1622164977=1.sql,D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=092257=aCP-1622164977=2.sql,D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=092257=aCP-1622164977=3.sql,D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=092257=aCP-1622164977=4.sql,D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=092257=aCP-1622164977=5.sql,D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=092257=aCP-1622164977=6.sql', 0, 1622164981, 1622164981);
INSERT INTO `cnpscy_table_backups` VALUES (5, 0, '127.0.0.1', 'cnpscy_admin_infos,cnpscy_admin_login_logs,cnpscy_admin_login_logs_2008,cnpscy_admin_logs,cnpscy_admin_menus,cnpscy_admin_menus_copy1,cnpscy_admin_menus_old,cnpscy_admin_role_with_menus,cnpscy_admin_roles,cnpscy_admin_with_roles,cnpscy_admins,cnpscy_adverts,cnpscy_article_categories,cnpscy_article_categorys,cnpscy_article_contents,cnpscy_article_datas,cnpscy_article_labels,cnpscy_article_with_labels,cnpscy_articles,cnpscy_banners,cnpscy_configs,cnpscy_failed_jobs,cnpscy_friendlinks,cnpscy_galleries,cnpscy_gallery_details,cnpscy_gallery_tags,cnpscy_gallery_with_tags,cnpscy_good_categories,cnpscy_help_centers,cnpscy_menus,cnpscy_migrations,cnpscy_notices,cnpscy_notifies,cnpscy_password_resets,cnpscy_protocols,cnpscy_system_notify_users,cnpscy_table_backups,cnpscy_upload_files,cnpscy_upload_groups,cnpscy_users,cnpscy_versions', 5931442, 1, 'D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=101935=zuF-1622168375=1.sql', 0, 1622168379, 1622168379);
INSERT INTO `cnpscy_table_backups` VALUES (6, 0, '127.0.0.1', 'cnpscy_admin_infos,cnpscy_admin_login_logs,cnpscy_admin_login_logs_2008,cnpscy_admin_logs,cnpscy_admin_menus,cnpscy_admin_menus_copy1,cnpscy_admin_menus_old,cnpscy_admin_role_with_menus,cnpscy_admin_roles,cnpscy_admin_with_roles,cnpscy_admins,cnpscy_adverts,cnpscy_article_categories,cnpscy_article_categorys,cnpscy_article_contents,cnpscy_article_datas,cnpscy_article_labels,cnpscy_article_with_labels,cnpscy_articles,cnpscy_banners,cnpscy_configs,cnpscy_failed_jobs,cnpscy_friendlinks,cnpscy_galleries,cnpscy_gallery_details,cnpscy_gallery_tags,cnpscy_gallery_with_tags,cnpscy_good_categories,cnpscy_help_centers,cnpscy_menus,cnpscy_migrations,cnpscy_notices,cnpscy_notifies,cnpscy_password_resets,cnpscy_protocols,cnpscy_system_notify_users,cnpscy_table_backups,cnpscy_upload_files,cnpscy_upload_groups,cnpscy_users,cnpscy_versions', 5932635, 1, 'D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=102038=SAv-1622168438=1.sql', 0, 1622168442, 1622168442);
INSERT INTO `cnpscy_table_backups` VALUES (7, 0, '127.0.0.1', 'cnpscy_admin_infos,cnpscy_admin_login_logs,cnpscy_admin_login_logs_2008,cnpscy_admin_logs,cnpscy_admin_menus,cnpscy_admin_menus_copy1,cnpscy_admin_menus_old,cnpscy_admin_role_with_menus,cnpscy_admin_roles,cnpscy_admin_with_roles,cnpscy_admins,cnpscy_adverts,cnpscy_article_categories,cnpscy_article_categorys,cnpscy_article_contents,cnpscy_article_datas,cnpscy_article_labels,cnpscy_article_with_labels,cnpscy_articles,cnpscy_banners,cnpscy_configs,cnpscy_failed_jobs,cnpscy_friendlinks,cnpscy_galleries,cnpscy_gallery_details,cnpscy_gallery_tags,cnpscy_gallery_with_tags,cnpscy_good_categories,cnpscy_help_centers,cnpscy_menus,cnpscy_migrations,cnpscy_notices,cnpscy_notifies,cnpscy_password_resets,cnpscy_protocols,cnpscy_system_notify_users,cnpscy_table_backups,cnpscy_upload_files,cnpscy_upload_groups,cnpscy_users,cnpscy_versions', 5933828, 1, 'D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=105934=3CL-1622170774=1.sql', 0, 1622170778, 1622170778);
INSERT INTO `cnpscy_table_backups` VALUES (8, 0, '127.0.0.1', 'cnpscy_admin_infos,cnpscy_admin_login_logs,cnpscy_admin_login_logs_2008,cnpscy_admin_logs,cnpscy_admin_menus,cnpscy_admin_menus_copy1,cnpscy_admin_menus_old,cnpscy_admin_role_with_menus,cnpscy_admin_roles,cnpscy_admin_with_roles,cnpscy_admins,cnpscy_adverts,cnpscy_article_categories,cnpscy_article_categorys,cnpscy_article_contents,cnpscy_article_datas,cnpscy_article_labels,cnpscy_article_with_labels,cnpscy_articles,cnpscy_banners,cnpscy_configs,cnpscy_failed_jobs,cnpscy_friendlinks,cnpscy_galleries,cnpscy_gallery_details,cnpscy_gallery_tags,cnpscy_gallery_with_tags,cnpscy_good_categories,cnpscy_help_centers,cnpscy_menus,cnpscy_migrations,cnpscy_notices,cnpscy_notifies,cnpscy_password_resets,cnpscy_protocols,cnpscy_system_notify_users,cnpscy_table_backups,cnpscy_upload_files,cnpscy_upload_groups,cnpscy_users,cnpscy_versions', 5935021, 1, 'D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=110007=Gte-1622170807=1.sql', 0, 1622170812, 1622170812);
INSERT INTO `cnpscy_table_backups` VALUES (9, 0, '127.0.0.1', 'cnpscy_admin_infos', 3499, 1, 'D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=110325=cfn-1622171005=1.sql', 0, 1622171005, 1622171005);
INSERT INTO `cnpscy_table_backups` VALUES (10, 0, '127.0.0.1', 'cnpscy_admin_infos', 3499, 1, 'D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=110451=yKb-1622171091=1.sql', 0, 1622171091, 1622171091);
INSERT INTO `cnpscy_table_backups` VALUES (15, 0, '127.0.0.1', 'cnpscy_admin_infos,cnpscy_admin_login_logs,cnpscy_admin_login_logs_2008,cnpscy_admin_logs,cnpscy_admin_menus,cnpscy_admin_menus_copy1,cnpscy_admin_menus_old,cnpscy_admin_role_with_menus,cnpscy_admin_roles,cnpscy_admin_with_roles,cnpscy_admins,cnpscy_adverts,cnpscy_article_categories,cnpscy_article_categorys,cnpscy_article_contents,cnpscy_article_datas,cnpscy_article_labels,cnpscy_article_with_labels,cnpscy_articles,cnpscy_banners,cnpscy_configs,cnpscy_failed_jobs,cnpscy_friendlinks,cnpscy_galleries,cnpscy_gallery_details,cnpscy_gallery_tags,cnpscy_gallery_with_tags,cnpscy_good_categories,cnpscy_help_centers,cnpscy_menus,cnpscy_migrations,cnpscy_notices,cnpscy_notifies,cnpscy_password_resets,cnpscy_protocol_categories,cnpscy_protocols,cnpscy_system_notify_users,cnpscy_table_backups,cnpscy_upload_files,cnpscy_upload_groups,cnpscy_users,cnpscy_versions', 1508598, 1, 'D:\\phpEnv\\www\\cnpscy\\laravel-vue-admin/database/backups/laravel-vue-admin=2021-05-28=162121=rcm-1622190081=1.sql', 0, 1622190083, 1622190083);

-- ----------------------------
-- Table structure for cnpscy_upload_files
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_upload_files`;
CREATE TABLE `cnpscy_upload_files`  (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员Id',
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分组Id',
  `storage` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '存储方式',
  `host_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '存储域名',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件路径',
  `file_size` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小(字节)',
  `file_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件类型',
  `extension` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件扩展名',
  `is_delete` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '软删除',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`file_id`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 150 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员的所有图片记录表 == upload_file' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_upload_files
-- ----------------------------
INSERT INTO `cnpscy_upload_files` VALUES (1, 303, 0, 'local', '', '20200721151320186094688.jpg', 4214, 'image', 'jpg', 1, 1596092426, 1621587286);
INSERT INTO `cnpscy_upload_files` VALUES (2, 303, 0, 'local', '', '202007211530357358b6999.jpg', 18164, 'image', 'jpg', 1, 1596092426, 1596092426);
INSERT INTO `cnpscy_upload_files` VALUES (3, 303, 0, 'local', '', '202007211536264dbdb4798.jpg', 4214, 'image', 'jpg', 1, 1596092426, 1596092426);
INSERT INTO `cnpscy_upload_files` VALUES (4, 303, 0, 'local', '', '20200721153721874954415.jpg', 18164, 'image', 'jpg', 1, 1596092426, 1596092426);
INSERT INTO `cnpscy_upload_files` VALUES (5, 288, 0, 'local', '', 'avatar/288_avatar.jpg', 5418, 'image', 'jpg', 1, 1596768802, 1596768802);
INSERT INTO `cnpscy_upload_files` VALUES (6, 304, 0, 'local', '', 'avatar/304_avatar.jpg', 2153, 'image', 'jpg', 1, 1596768837, 1596768837);
INSERT INTO `cnpscy_upload_files` VALUES (7, 305, 0, 'local', '', 'avatar/305_avatar.jpg', 5416, 'image', 'jpg', 1, 1596770280, 1596770280);
INSERT INTO `cnpscy_upload_files` VALUES (8, 306, 0, 'local', '', 'avatar/306_avatar.jpg', 5413, 'image', 'jpg', 1, 1596770599, 1596770599);
INSERT INTO `cnpscy_upload_files` VALUES (9, 1000000, 0, 'local', '', 'avatar/1000000_avatar.jpg', 2151, 'image', 'jpg', 1, 1596781612, 1596781612);
INSERT INTO `cnpscy_upload_files` VALUES (10, 1000001, 0, 'local', '', 'avatar/1000001_avatar.jpg', 5413, 'image', 'jpg', 1, 1596781735, 1596781735);
INSERT INTO `cnpscy_upload_files` VALUES (11, 1000002, 0, 'local', '', 'avatar/1000002_avatar.jpg', 2152, 'image', 'jpg', 1, 1596785875, 1596785875);
INSERT INTO `cnpscy_upload_files` VALUES (12, 1000003, 0, 'local', '', 'avatar/1000003_avatar.jpg', 2153, 'image', 'jpg', 1, 1596787022, 1621588694);
INSERT INTO `cnpscy_upload_files` VALUES (13, 1000004, 0, 'local', '', 'avatar/1000004_avatar.jpg', 2152, 'image', 'jpg', 1, 1596787045, 1621588694);
INSERT INTO `cnpscy_upload_files` VALUES (14, 1000005, 10038, 'local', '', 'avatar/1000005_avatar.jpg', 2153, 'image', 'jpg', 1, 1597285510, 1621670483);
INSERT INTO `cnpscy_upload_files` VALUES (15, 1000006, 0, 'local', '', 'avatar/1000006_avatar.jpg', 4229, 'image', 'jpg', 1, 1597371772, 1597371772);
INSERT INTO `cnpscy_upload_files` VALUES (16, 1000006, 0, 'local', '', '20200814162353b3f3c5325.jpg', 18164, 'image', 'jpg', 1, 1597393433, 1597393433);
INSERT INTO `cnpscy_upload_files` VALUES (17, 1000006, 0, 'local', '', '20200814162404ab9f19852.jpg', 18164, 'image', 'jpg', 1, 1597393444, 1597393444);
INSERT INTO `cnpscy_upload_files` VALUES (18, 1000006, 0, 'local', '', '202008141624046063a0839.png', 21476, 'image', 'png', 1, 1597393444, 1597393444);
INSERT INTO `cnpscy_upload_files` VALUES (19, 1000006, 0, 'local', '', '20200814162404d3d087379.png', 75074, 'image', 'png', 1, 1597393444, 1597393444);
INSERT INTO `cnpscy_upload_files` VALUES (20, 1000006, 0, 'local', '', '20200814162543b79507785.jpg', 18164, 'image', 'jpg', 1, 1597393543, 1597393543);
INSERT INTO `cnpscy_upload_files` VALUES (21, 1000006, 0, 'local', '', '2020081416392995d199074.jpg', 18164, 'image', 'jpg', 1, 1597394369, 1597394369);
INSERT INTO `cnpscy_upload_files` VALUES (22, 1000006, 0, 'local', '', '202008141639292c8df5123.png', 21476, 'image', 'png', 1, 1597394369, 1597394369);
INSERT INTO `cnpscy_upload_files` VALUES (23, 1000006, 0, 'local', '', '20200814164209a78f66694.jpg', 18164, 'image', 'jpg', 1, 1597394529, 1597394529);
INSERT INTO `cnpscy_upload_files` VALUES (24, 1000006, 0, 'local', '', '202008141642361dc6d5448.jpg', 18164, 'image', 'jpg', 1, 1597394556, 1597394556);
INSERT INTO `cnpscy_upload_files` VALUES (25, 1000006, 0, 'local', '', '20200814164302de3d59795.jpg', 18164, 'image', 'jpg', 1, 1597394582, 1597394582);
INSERT INTO `cnpscy_upload_files` VALUES (26, 1000006, 0, 'local', '', '202008141643282d8c59848.jpg', 18164, 'image', 'jpg', 1, 1597394608, 1597394608);
INSERT INTO `cnpscy_upload_files` VALUES (27, 1000006, 0, 'local', '', '202008141644146e2c45227.jpg', 18164, 'image', 'jpg', 1, 1597394654, 1597394654);
INSERT INTO `cnpscy_upload_files` VALUES (28, 1000006, 0, 'local', '', '202008141644406c4894346.jpg', 18164, 'image', 'jpg', 1, 1597394680, 1597394680);
INSERT INTO `cnpscy_upload_files` VALUES (29, 1000006, 0, 'local', '', '20200814164515926d41534.jpg', 18164, 'image', 'jpg', 1, 1597394715, 1597394715);
INSERT INTO `cnpscy_upload_files` VALUES (30, 1000006, 0, 'local', '', '202008141645427edf57461.jpg', 18164, 'image', 'jpg', 1, 1597394742, 1597394742);
INSERT INTO `cnpscy_upload_files` VALUES (31, 1000006, 0, 'local', '', '20200814164657f08678138.jpg', 18164, 'image', 'jpg', 1, 1597394817, 1597394817);
INSERT INTO `cnpscy_upload_files` VALUES (32, 1000006, 0, 'local', '', '20200814164714c65dd2128.jpg', 18164, 'image', 'jpg', 1, 1597394834, 1597394834);
INSERT INTO `cnpscy_upload_files` VALUES (33, 1000006, 0, 'local', '', '20200814164714ae2cf3315.png', 21476, 'image', 'png', 1, 1597394834, 1597394834);
INSERT INTO `cnpscy_upload_files` VALUES (34, 1000006, 0, 'local', '', '202008141647145f5c34453.png', 4539, 'image', 'png', 1, 1597394834, 1597394834);
INSERT INTO `cnpscy_upload_files` VALUES (35, 1000006, 0, 'local', '', '20200814164714faf7c5762.png', 75074, 'image', 'png', 1, 1597394834, 1597394834);
INSERT INTO `cnpscy_upload_files` VALUES (36, 1000006, 0, 'local', '', '20200814164714e96839301.png', 61241, 'image', 'png', 1, 1597394834, 1597394834);
INSERT INTO `cnpscy_upload_files` VALUES (37, 1000006, 0, 'local', '', '20200814165243766660710.png', 21476, 'image', 'png', 1, 1597395163, 1597395163);
INSERT INTO `cnpscy_upload_files` VALUES (38, 1000006, 0, 'local', '', '202008141652437d6a10052.jpg', 18164, 'image', 'jpg', 1, 1597395163, 1597395163);
INSERT INTO `cnpscy_upload_files` VALUES (39, 1000006, 0, 'local', '', '20200814165243f2dd17465.png', 75074, 'image', 'png', 1, 1597395163, 1597395163);
INSERT INTO `cnpscy_upload_files` VALUES (40, 1000006, 0, 'local', '', '20200814171021ee42e9552.png', 10043, 'image', 'png', 1, 1597396221, 1597396221);
INSERT INTO `cnpscy_upload_files` VALUES (41, 1000006, 0, 'local', '', '2020081417102148aa59287.png', 4539, 'image', 'png', 1, 1597396221, 1597396221);
INSERT INTO `cnpscy_upload_files` VALUES (42, 1000006, 0, 'local', '', '20200814171021f60a38836.JPG', 127430, 'image', 'JPG', 1, 1597396221, 1597396221);
INSERT INTO `cnpscy_upload_files` VALUES (43, 1000006, 0, 'local', '', '202008141710211df418031.png', 25584, 'image', 'png', 1, 1597396221, 1597396221);
INSERT INTO `cnpscy_upload_files` VALUES (44, 1000006, 0, 'local', '', '20200814171021f6faf5384.png', 61241, 'image', 'png', 1, 1597396221, 1597396221);
INSERT INTO `cnpscy_upload_files` VALUES (45, 1000006, 0, 'local', '', '20200814171021923859806.png', 26114, 'image', 'png', 1, 1597396221, 1597396221);
INSERT INTO `cnpscy_upload_files` VALUES (46, 1000006, 0, 'local', '', '20200814171509b847c4176.jpg', 18164, 'image', 'jpg', 1, 1597396509, 1597396509);
INSERT INTO `cnpscy_upload_files` VALUES (47, 1000006, 0, 'local', '', '20200814171509593a53898.png', 21476, 'image', 'png', 1, 1597396509, 1597396509);
INSERT INTO `cnpscy_upload_files` VALUES (48, 1000006, 0, 'local', '', '20200814171509caaa20585.JPG', 127430, 'image', 'JPG', 1, 1597396509, 1597396509);
INSERT INTO `cnpscy_upload_files` VALUES (49, 1000006, 0, 'local', '', '20200814171509d34a53686.png', 4539, 'image', 'png', 1, 1597396509, 1597396509);
INSERT INTO `cnpscy_upload_files` VALUES (50, 1000006, 0, 'local', '', '20200814171509962223939.png', 75074, 'image', 'png', 1, 1597396509, 1597396509);
INSERT INTO `cnpscy_upload_files` VALUES (51, 1000006, 0, 'local', '', '20200817112057e00096429.jpg', 18164, 'image', 'jpg', 1, 1597634457, 1597634457);
INSERT INTO `cnpscy_upload_files` VALUES (52, 1000006, 0, 'local', '', '2020081711320174ad15257.jpg', 18164, 'image', 'jpg', 1, 1597635121, 1597635121);
INSERT INTO `cnpscy_upload_files` VALUES (53, 1000006, 0, 'local', '', '20200817153224ef73b2876.mp4', 1931104, 'image', 'mp4', 1, 1597649544, 1597649544);
INSERT INTO `cnpscy_upload_files` VALUES (54, 1000006, 0, 'local', '', '202008171538174d5a22258.mp4', 1931104, 'image', 'mp4', 1, 1597649897, 1597649897);
INSERT INTO `cnpscy_upload_files` VALUES (55, 1000006, 0, 'local', '', '20200817154516191355512.mp4', 1931104, 'image', 'mp4', 1, 1597650316, 1597650316);
INSERT INTO `cnpscy_upload_files` VALUES (56, 1000006, 0, 'local', '', '20200817154553c8dc24230.mp4', 1931104, 'image', 'mp4', 1, 1597650353, 1597650353);
INSERT INTO `cnpscy_upload_files` VALUES (57, 1000006, 0, 'local', '', '20200817154844bc5415047.mp4', 1931104, 'image', 'mp4', 1, 1597650524, 1597650524);
INSERT INTO `cnpscy_upload_files` VALUES (58, 1000006, 0, 'local', '', '20200817154927d0bb93157.mp4', 1931104, 'image', 'mp4', 1, 1597650567, 1597650567);
INSERT INTO `cnpscy_upload_files` VALUES (59, 1000006, 0, 'local', '', '20200817155223a8ac72030.mp4', 1931104, 'image', 'mp4', 1, 1597650743, 1597650743);
INSERT INTO `cnpscy_upload_files` VALUES (60, 1000006, 0, 'local', '', '20200817155508be67f3510.mp4', 1931104, 'image', 'mp4', 1, 1597650908, 1597650908);
INSERT INTO `cnpscy_upload_files` VALUES (61, 1000006, 0, 'local', '', '2020081716135541cce1659.JPG', 127430, 'image', 'JPG', 1, 1597652035, 1597652035);
INSERT INTO `cnpscy_upload_files` VALUES (62, 1000006, 0, 'local', '', '202008171617427ffc52885.jpg', 18164, 'image', 'jpg', 1, 1597652262, 1597652262);
INSERT INTO `cnpscy_upload_files` VALUES (63, 1000006, 0, 'local', '', '20200817161834e1eeb2941.jpg', 18164, 'image', 'jpg', 1, 1597652314, 1597652314);
INSERT INTO `cnpscy_upload_files` VALUES (64, 1000006, 0, 'local', '', '2020081716235365e5a2744.jpg', 18164, 'image', 'jpg', 1, 1597652633, 1597652633);
INSERT INTO `cnpscy_upload_files` VALUES (65, 1000006, 0, 'local', '', '20200817162353f13f11326.jpg', 18164, 'image', 'jpg', 1, 1597652633, 1597652633);
INSERT INTO `cnpscy_upload_files` VALUES (66, 1000006, 0, 'local', '', '20200817162353839dc6604.jpg', 18164, 'image', 'jpg', 1, 1597652633, 1597652633);
INSERT INTO `cnpscy_upload_files` VALUES (67, 1000006, 0, 'local', '', '202008171623538daa82005.JPG', 127430, 'image', 'JPG', 1, 1597652633, 1597652633);
INSERT INTO `cnpscy_upload_files` VALUES (68, 1000006, 0, 'local', '', '20200817162353cabeb6754.JPG', 127430, 'image', 'JPG', 1, 1597652633, 1597652633);
INSERT INTO `cnpscy_upload_files` VALUES (69, 1000006, 0, 'local', '', '20200817162353d0cc25756.JPG', 127430, 'image', 'JPG', 1, 1597652633, 1597652633);
INSERT INTO `cnpscy_upload_files` VALUES (70, 1000005, 0, 'local', '', '202008171639314c87b4343.jpg', 18164, 'image', 'jpg', 1, 1597653571, 1597653571);
INSERT INTO `cnpscy_upload_files` VALUES (71, 1000006, 0, 'local', '', '20200817185350f5cea3217.JPG', 127430, 'image', 'JPG', 1, 1597661630, 1597661630);
INSERT INTO `cnpscy_upload_files` VALUES (72, 0, 0, 'local', '', '/public/uploads/2020-08-21/boNGDbE9EbQnaAnkfAq8mHxNDdlQSs1598002884.png', 13280, 'image/png', 'png', 1, 1598002884, 1598002884);
INSERT INTO `cnpscy_upload_files` VALUES (73, 0, 0, 'local', '', '/uploads/2020-08-21/kLwFJdLE2i2wd7ohveLF-1598003456.png', 13280, 'image/png', 'png', 1, 1598003456, 1598003456);
INSERT INTO `cnpscy_upload_files` VALUES (74, 0, 0, 'local', '', '/uploads/2020-08-21/Psx4zEjNHDGt1QD4S94l-1598003486.png', 13280, 'image/png', 'png', 1, 1598003486, 1598003486);
INSERT INTO `cnpscy_upload_files` VALUES (75, 0, 0, 'local', '', '/uploads/2020-08-21/Nb8j7u9mEEJ5flQGlEQk-1598003524.png', 13280, 'image/png', 'png', 1, 1598003524, 1598003524);
INSERT INTO `cnpscy_upload_files` VALUES (76, 0, 0, 'local', '', '/uploads/2020-08-21/P6S7rKj2CfzAm2QsJbT1-1598003567.png', 13280, 'image/png', 'png', 1, 1598003567, 1598003567);
INSERT INTO `cnpscy_upload_files` VALUES (77, 0, 0, 'local', '', '/uploads/2020-08-21/9l7Cb33ocQPxiPqunED3-1598004503.png', 13280, 'image/png', 'png', 1, 1598004503, 1598004503);
INSERT INTO `cnpscy_upload_files` VALUES (78, 0, 0, 'local', '', '/uploads/2020-08-26/AlK8pzAknlaEwBE5d3lC-1598426786.png', 5673, 'image/png', 'png', 1, 1598426786, 1598426786);
INSERT INTO `cnpscy_upload_files` VALUES (79, 0, 0, 'local', '', '/uploads/2020-08-26/rAplwDMrElnj6yKmjPv5-1598427282.png', 5673, 'image/png', 'png', 1, 1598427282, 1598427282);
INSERT INTO `cnpscy_upload_files` VALUES (80, 0, 0, 'local', '', '/uploads/2020-08-26/dPimeChgoG2LGjFHsdEN-1598427843.png', 5673, 'image/png', 'png', 1, 1598427843, 1598427843);
INSERT INTO `cnpscy_upload_files` VALUES (81, 0, 0, 'local', '', '/uploads/2020-08-26/EjhSpwmEwsuvlo2x7Jjx-1598428810.png', 5673, 'image/png', 'png', 1, 1598428810, 1598428810);
INSERT INTO `cnpscy_upload_files` VALUES (82, 0, 0, 'local', '', '/uploads/2020-08-26/3q6vod6thdi76hzvT6iN-1598437103.png', 2592, 'image/png', 'png', 1, 1598437103, 1598437103);
INSERT INTO `cnpscy_upload_files` VALUES (83, 0, 0, 'local', '', '/uploads/2020-08-26/zikcFulbpBCS5bfgBHSi-1598437252.png', 2592, 'image/png', 'png', 1, 1598437252, 1598437252);
INSERT INTO `cnpscy_upload_files` VALUES (84, 0, 0, 'local', '', '/uploads/2020-08-26/ALBlgT6T6dr8lEe9eAxx-1598438557.png', 2592, 'image/png', 'png', 1, 1598438557, 1598438557);
INSERT INTO `cnpscy_upload_files` VALUES (85, 0, 0, 'local', '', '/uploads/2020-08-27/dfjkavMkiymwwQMaeNEv-1598494730.png', 2592, 'image/png', 'png', 1, 1598494730, 1598494730);
INSERT INTO `cnpscy_upload_files` VALUES (86, 0, 0, 'local', '', '/uploads/2020-08-27/1a5CG8dcKGppnf6rpHxp-1598511659.png', 3687, 'image/png', 'png', 1, 1598511659, 1598511659);
INSERT INTO `cnpscy_upload_files` VALUES (87, 0, 0, 'local', '', '/uploads/2020-08-27/d5pGftqgqto6dS87mBNS-1598511694.png', 3687, 'image/png', 'png', 1, 1598511694, 1598511694);
INSERT INTO `cnpscy_upload_files` VALUES (88, 0, 0, 'local', '', '/uploads/2020-08-27/kNQstELDNmyqtMjkoE3L-1598512001.png', 6947, 'image/png', 'png', 1, 1598512001, 1598512001);
INSERT INTO `cnpscy_upload_files` VALUES (89, 0, 0, 'local', '', '/uploads/2020-08-31/s2bzE3x7NsNjGDixGFxo-1598858615png', 10, 'image/png', 'png', 1, 1598858615, 1598858615);
INSERT INTO `cnpscy_upload_files` VALUES (90, 0, 0, 'local', '', '/uploads/2020-08-31/TgPuuqJFbnfBAEsiHNsn-1598858812png', 10, 'image/png', 'png', 1, 1598858812, 1598858812);
INSERT INTO `cnpscy_upload_files` VALUES (91, 0, 0, 'local', '', '/uploads/2020-08-31/tEDK6BbM6nwQbBeEj3oj-1598858872png', 10, 'image/png', 'png', 1, 1598858872, 1598858872);
INSERT INTO `cnpscy_upload_files` VALUES (92, 0, 0, 'local', '', '/uploads/2020-08-31/MAQHSuzmihC8FKdizmj6-1598858931png', 10, 'image/png', 'png', 1, 1598858931, 1598858931);
INSERT INTO `cnpscy_upload_files` VALUES (93, 0, 0, 'local', '', '/uploads/2020-08-31/xqxLPL8hhHfzDQzC9jKy-1598859003png', 10, 'image/png', 'png', 1, 1598859004, 1598859004);
INSERT INTO `cnpscy_upload_files` VALUES (94, 0, 0, 'local', '', '/uploads/2020-08-31/6vbdAzL8bEwQ52lToK2M-1598859117png', 10, 'image/png', 'png', 1, 1598859117, 1598859117);
INSERT INTO `cnpscy_upload_files` VALUES (95, 0, 0, 'local', '', '/uploads/2020-08-31/9482vbiNE5tMEE9QThcL-1598859247.png', 10, 'image/png', 'png', 1, 1598859247, 1598859247);
INSERT INTO `cnpscy_upload_files` VALUES (96, 0, 0, 'local', '', '/uploads/2020-08-31/a5DQgMdnb2m32Md7rc3F-1598864904.png', 3155, 'image/png', 'png', 1, 1598864904, 1598864904);
INSERT INTO `cnpscy_upload_files` VALUES (97, 0, 0, '', '', '/uploads/logo.jpg', 0, '', '', 1, 0, 0);
INSERT INTO `cnpscy_upload_files` VALUES (98, 0, 0, '', '', '/uploads/avatar.jpg', 0, '', '', 1, 0, 0);
INSERT INTO `cnpscy_upload_files` VALUES (99, 1, 0, 'local', '', '/uploads/2020-09-18/GHlnscg1nEBgTbPBCp3w-1600423916.jpg', 15733, 'image/jpeg', 'jpg', 1, 1600423916, 1600423916);
INSERT INTO `cnpscy_upload_files` VALUES (100, 1, 0, 'local', '', '/uploads/2020-09-18/nsti73AmL3oEABmqp7AS-1600424032.jpg', 15733, 'image/jpeg', 'jpg', 1, 1600424032, 1600424032);
INSERT INTO `cnpscy_upload_files` VALUES (101, 1, 0, 'local', '', '/uploads/2020-09-18/uldsCQLypJMQyb79PSjP-1600424048.jpg', 15733, 'image/jpeg', 'jpg', 1, 1600424048, 1600424048);
INSERT INTO `cnpscy_upload_files` VALUES (102, 1, 0, 'local', '', '/uploads/2020-09-18/S2xHwqu1gNyQgdenx12r-1600424073.jpg', 15733, 'image/jpeg', 'jpg', 1, 1600424073, 1600424073);
INSERT INTO `cnpscy_upload_files` VALUES (103, 1, 0, 'local', '', '/uploads/2020-09-18/gcqPoGCNgqPsq3ezvgxj-1600424088.jpg', 15733, 'image/jpeg', 'jpg', 1, 1600424088, 1600424088);
INSERT INTO `cnpscy_upload_files` VALUES (104, 1, 0, 'local', '', '/uploads/2020-09-18/2E3tHozT9E5FSE9CnDDQ-1600424150.jpg', 15733, 'image/jpeg', 'jpg', 1, 1600424150, 1600424150);
INSERT INTO `cnpscy_upload_files` VALUES (105, 1, 0, 'local', '', '/uploads/2020-09-18/EMuabnhCrs4H2xks12J9-1600424184.jpg', 15733, 'image/jpeg', 'jpg', 1, 1600424184, 1600424184);
INSERT INTO `cnpscy_upload_files` VALUES (106, 1, 0, 'local', '', '/uploads/2020-09-18/cxQ7cogjoeJFdiPmo4k1-1600424267.png', 1363665, 'image/png', 'png', 1, 1600424267, 1600424267);
INSERT INTO `cnpscy_upload_files` VALUES (107, 1, 0, 'local', '', '/uploads/2020-09-18/KClK3x5sFhpsyJzS8rr4-1600424547.jpg', 15733, 'image/jpeg', 'jpg', 1, 1600424547, 1600424547);
INSERT INTO `cnpscy_upload_files` VALUES (108, 1, 0, 'local', '', '/uploads/2020-09-18/PFmBpigqQ3se8Bf4cAug-1600424615.png', 1363665, 'image/png', 'png', 1, 1600424615, 1600424615);
INSERT INTO `cnpscy_upload_files` VALUES (109, 1, 0, 'local', '', '/uploads/2020-09-18/jakQAKa3Mw5lTPrnv9L6-1600424929.jpg', 4214, 'image/jpeg', 'jpg', 1, 1600424929, 1600424929);
INSERT INTO `cnpscy_upload_files` VALUES (110, 1, 0, 'local', '', '/uploads/2020-09-27/L3FlSn8B9M6EJwtMgFcK-1601178349.jpg', 15733, 'image/jpeg', 'jpg', 1, 1601178349, 1601178349);
INSERT INTO `cnpscy_upload_files` VALUES (111, 0, 0, 'local', '', '/uploads/2020-09-27/2F8EFCSkArgS7xJK795z-1601189104.png', 8, 'image/png', 'jpg', 1, 1601189104, 1601189104);
INSERT INTO `cnpscy_upload_files` VALUES (112, 0, 0, 'local', '', '/uploads/2020-09-27/pd6tnre4l4pKeKcFlAwp-1601189104.png', 8, 'image/png', 'jpg', 1, 1601189104, 1601189104);
INSERT INTO `cnpscy_upload_files` VALUES (113, 0, 0, 'local', '', '/uploads/2020-09-27/v31jHnmHL1f2aSB2h4ye-1601189104.png', 8, 'image/png', 'jpg', 1, 1601189104, 1601189104);
INSERT INTO `cnpscy_upload_files` VALUES (114, 0, 0, 'local', '', '/uploads/2020-09-27/Dwvcer69cqn2ohMoseP6-1601189104.png', 8, 'image/png', 'jpg', 1, 1601189104, 1601189104);
INSERT INTO `cnpscy_upload_files` VALUES (115, 0, 0, 'local', '', '/uploads/2020-09-27/f2cwjr4o8t3woPENzC9J-1601189104.png', 8, 'image/png', 'jpg', 1, 1601189104, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (116, 0, 0, 'local', '', '/uploads/2020-09-27/5Krk1MzA2QjCvtuoxN29-1601189105.png', 8, 'image/png', 'jpg', 1, 1601189105, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (117, 0, 0, 'local', '', '/uploads/2020-09-27/N5PwHJ4kHjmub48T5DgA-1601189105.png', 8, 'image/png', 'jpg', 1, 1601189105, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (118, 0, 0, 'local', '', '/uploads/2020-09-27/eKEh24qEJwrnh6QqG5Bj-1601189105.png', 8, 'image/png', 'jpg', 1, 1601189105, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (119, 0, 0, 'local', '', '/uploads/2020-09-27/K4yMDEaKh8wTJCCpboxf-1601189105.png', 8, 'image/png', 'jpg', 1, 1601189105, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (120, 0, 0, 'local', '', '/uploads/2020-09-27/CTLb6PNCkTDh1JcKbw4c-1601189105.png', 8, 'image/png', 'jpg', 1, 1601189105, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (121, 0, 0, 'local', '', '/uploads/2020-09-27/ziA8r9m5qzG2AFomo66d-1601199775.png', 8, 'image/png', 'jpg', 1, 1601199775, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (122, 0, 0, 'local', '', '/uploads/2020-09-27/D6Ce7LJHwN7vNuT2vD4D-1601199775.png', 8, 'image/png', 'jpg', 1, 1601199775, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (123, 0, 0, 'local', '', '/uploads/2020-09-27/o5a3t7zjeDDKaKEEkzL4-1601199775.png', 8, 'image/png', 'jpg', 1, 1601199775, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (124, 0, 0, 'local', '', '/uploads/2020-09-27/xAynuwmTEom5gw4SgS82-1601199775.png', 8, 'image/png', 'jpg', 1, 1601199775, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (125, 0, 0, 'local', '', '/uploads/2020-09-27/QxpAGiBjJ6TkFwqHE4Kp-1601199775.png', 8, 'image/png', 'jpg', 1, 1601199775, 1621588568);
INSERT INTO `cnpscy_upload_files` VALUES (126, 0, 0, 'local', '', '/uploads/2020-09-27/AdDEt5C9SoAwi35TFC7j-1601199775.png', 8, 'image/png', 'jpg', 1, 1601199775, 1621588556);
INSERT INTO `cnpscy_upload_files` VALUES (127, 0, 0, 'local', '', '/uploads/2020-09-27/8iC7P33MoAEzPyJAJuxP-1601199775.png', 8, 'image/png', 'jpg', 1, 1601199775, 1621587705);
INSERT INTO `cnpscy_upload_files` VALUES (128, 0, 0, 'local', '', '/uploads/2020-09-27/xlqmMFejpDtNLBMwHCjC-1601199775.png', 8, 'image/png', 'jpg', 1, 1601199775, 1621587716);
INSERT INTO `cnpscy_upload_files` VALUES (129, 0, 0, 'local', '', '/uploads/2020-09-27/7fgFoQH5zeEcrJ6k8CgH-1601199775.png', 8, 'image/png', 'jpg', 1, 1601199775, 1621588556);
INSERT INTO `cnpscy_upload_files` VALUES (130, 0, 0, 'local', '', '/uploads/2020-09-27/odnwEmBKsP6jnLbhNwTS-1601199775.png', 8, 'image/png', 'jpg', 1, 1601199775, 1621588556);
INSERT INTO `cnpscy_upload_files` VALUES (131, 0, 10037, 'local', '', '202105/54AD4BLJ6iEqq4DxXlh6u0eSuWOgotG62nHKvTrW.jpg', 13975, 'image/jpeg', 'jpg', 0, 1621586270, 1621670813);
INSERT INTO `cnpscy_upload_files` VALUES (132, 0, 10040, 'local', '', '202105/Yxrj7i7OJVLw92jMlA42KvjNbbrJ1iE7af7hslOk.jpg', 13975, 'image/jpeg', 'jpg', 0, 1621586301, 1621838119);
INSERT INTO `cnpscy_upload_files` VALUES (133, 0, 0, 'local', '', '202105/OVf5dDPc94UHWQXZiqRXVxB0nL5VICMcc9LxGoNs.jpg', 13975, 'image/jpeg', 'jpg', 1, 1621586339, 1621588628);
INSERT INTO `cnpscy_upload_files` VALUES (134, 0, 10041, 'local', '', '202105/CxcAeijdPUqSeqryqd8PjrotRM3H3io2z07XoMdB.png', 158869, 'image/png', 'png', 0, 1621586703, 1621670196);
INSERT INTO `cnpscy_upload_files` VALUES (135, 0, 10038, 'local', '', '202105/pcZfhpUmDNauCm1CZBiz8jurcXwiunDVI8yxwJwr.jpg', 15733, 'image/jpeg', 'jpg', 0, 1621586713, 1621838110);
INSERT INTO `cnpscy_upload_files` VALUES (136, 0, 0, 'local', '', '202105/9vpT5JdhzojC5sSZT7rz2ptEb7zVFXLXTH4aUoCa.mp4', 1931104, 'video/mp4', 'mp4', 0, 1621586718, 1621586718);
INSERT INTO `cnpscy_upload_files` VALUES (137, 0, 0, 'local', '', '202105/YVjXcMSLdmMPtuYIQtY5SRLL5I00ZwLyn3WmaTAO.jpg', 240275, 'image/jpeg', 'jpg', 0, 1621586724, 1621586724);
INSERT INTO `cnpscy_upload_files` VALUES (138, 0, 0, 'local', '', '202105/PhWrfTOp2wx1GQsNye7wYRcZl6wNtHl9jyUXXZBC.txt', 32, 'text/plain', 'txt', 0, 1621591247, 1621591247);
INSERT INTO `cnpscy_upload_files` VALUES (139, 0, 0, 'local', '', '202105/LM6jSgpx3zhBwOlQzCpYPMKCMwBGhbVhjaM2uBRm.jpg', 240275, 'image/jpeg', 'jpg', 0, 1621591247, 1621591247);
INSERT INTO `cnpscy_upload_files` VALUES (140, 0, 0, 'local', '', '202105/yrM7S75HxdLQHaUn8rlu3REvkF1h4bYw7JRJeqIK.pdf', 426505, 'application/pdf', 'pdf', 0, 1621591248, 1621591248);
INSERT INTO `cnpscy_upload_files` VALUES (141, 0, 0, 'local', '', '202105/tBIZJoc4TbkAda0v38IWremvGGQhSCNmnjLgwgcF.mp3', 8430280, 'audio/mpeg', 'mp3', 0, 1621676454, 1621676454);
INSERT INTO `cnpscy_upload_files` VALUES (142, 0, 0, 'local', '', '202105/svqkmqgNnlbA8GeDGfujJlC0A6A9JxJxsfpTvY3l.zip', 6437, 'application/zip', 'zip', 0, 1621676461, 1621676461);
INSERT INTO `cnpscy_upload_files` VALUES (143, 0, 0, 'local', '', '202105/U7SAaR9wrMzqDmNIVcMg9mkG2fkjeOqUE2cCtNEl.pdf', 426505, 'application/pdf', 'pdf', 0, 1621676467, 1621676467);
INSERT INTO `cnpscy_upload_files` VALUES (144, 0, 0, 'local', '', '202105/btjfr944r6tsPvPvsE5uIfwRTc5KFMUOIUSWAh9B.wav', 80044, 'audio/x-wav', 'wav', 0, 1621676472, 1621676472);
INSERT INTO `cnpscy_upload_files` VALUES (145, 0, 0, 'local', '', '202105/LNFBqWyDQDdl9yRbGwahYCYCP9MDBJuA9aJOHJaG.txt', 748, 'text/plain', 'json', 0, 1621676479, 1621676479);
INSERT INTO `cnpscy_upload_files` VALUES (146, 0, 0, 'local', '', '202105/Yy1ZT53A7DIDqdDbL55uGBuIIbnvRQw5UzFW83Oq.html', 1075, 'text/html', 'html', 0, 1621676496, 1621676496);
INSERT INTO `cnpscy_upload_files` VALUES (147, 0, 0, 'local', '', '202105/JLKywZIn0SRRxufPQfJ7HmeFNC4LHUMs1tW6VNkf.txt', 88145, 'text/plain', 'js', 0, 1621676504, 1621676504);
INSERT INTO `cnpscy_upload_files` VALUES (148, 0, 0, 'local', '', '202105/0xYfXRLUPmvt1smPYt8oaG6luHcqck4a9HNuKYJe.txt', 2410, 'text/plain', 'sql', 0, 1621677335, 1621677335);
INSERT INTO `cnpscy_upload_files` VALUES (149, 0, 0, 'local', '', '202105/Gu6kcp3bQBn42z4gAUZ8qigyPezuclVaSCigaRCc', 3505, 'text/x-php', 'php', 0, 1621677357, 1621677357);

-- ----------------------------
-- Table structure for cnpscy_upload_groups
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_upload_groups`;
CREATE TABLE `cnpscy_upload_groups`  (
  `group_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `group_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件类型',
  `group_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `group_sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类排序(数字越小越靠前)',
  `is_delete` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除',
  `created_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`group_id`) USING BTREE,
  INDEX `type_index`(`group_type`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10042 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件库分组记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_upload_groups
-- ----------------------------
INSERT INTO `cnpscy_upload_groups` VALUES (10037, '', '1432432', 0, 0, 1621654881, 1621654881);
INSERT INTO `cnpscy_upload_groups` VALUES (10038, '', '2021-05', 0, 0, 1621655053, 1621655053);
INSERT INTO `cnpscy_upload_groups` VALUES (10039, '', '132131', 0, 1, 1621655186, 1621664514);
INSERT INTO `cnpscy_upload_groups` VALUES (10040, '', '112312', 0, 0, 1621661791, 1621662347);
INSERT INTO `cnpscy_upload_groups` VALUES (10041, '', '3', 0, 0, 1621662341, 1621663295);

-- ----------------------------
-- Table structure for cnpscy_users
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_users`;
CREATE TABLE `cnpscy_users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `cnpscy_users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cnpscy_versions
-- ----------------------------
DROP TABLE IF EXISTS `cnpscy_versions`;
CREATE TABLE `cnpscy_versions`  (
  `version_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '版本表',
  `version_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `version_number` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '版本号',
  `version_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '内容',
  `created_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updated_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除',
  `version_sort` smallint(3) UNSIGNED NULL DEFAULT 0 COMMENT '排序',
  `publish_date` datetime(0) NULL DEFAULT NULL COMMENT '版本的发布时间',
  PRIMARY KEY (`version_id`) USING BTREE,
  INDEX `cnpscy_versions_is_delete_index`(`is_delete`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cnpscy_versions
-- ----------------------------
INSERT INTO `cnpscy_versions` VALUES (1, '基础版本发布', '1.0.0', '[1.0.0 分支](https://gitee.com/clown-passerby-community/laravel-vue-admin/tree/1.0.0)', 1610002630, 1610010586, 0, 1, '2021-01-05 16:56:32');
INSERT INTO `cnpscy_versions` VALUES (2, '1.0.1', '1.0.1', '[1.0.1 分支](https://gitee.com/clown-passerby-community/laravel-vue-admin/tree/1.0.1)\n\n* **文章详情完善**；\n* **文章列表：状态变更设置；**\n* **默认图片组件的border-radius可随意传参设置;**\n* **系统设置下的配置管理、友情链接、Banner文件目录更换至根目录。**', 1610002657, 1610010613, 0, 2, '2021-01-06 00:03:18');
INSERT INTO `cnpscy_versions` VALUES (3, '1.0.2', '1.0.2', '[1.0.2 分支](https://gitee.com/clown-passerby-community/laravel-vue-admin/tree/1.0.2)\n\n* **表单内的下拉框，默认100%宽度占比；**\n* **表单详情排版label不同页面不同宽度调整；**\n* **列表页状态对应图标展示效果；**\n* **scope.row替换为row简化代码写法；**\n* **菜单管理列表页：隐藏与展示图标对应效果。**', 1610002669, 1610010640, 0, 3, '2021-01-06 13:03:24');
INSERT INTO `cnpscy_versions` VALUES (4, '1.0.3', '1.0.3', '[1.0.3 分支](https://gitee.com/clown-passerby-community/laravel-vue-admin/tree/1.0.3)\n\n* **版本管理；**\n* **form input 宽度100%设定；**\n* **引入 moment；**\n* **vue-element-admin Markdown 输出样式排版问题修复方案；**\n* **首页：版本历史记录渲染；**\n* **markdown 渲染样式。**', 1610095906, 1610096043, 0, 4, '2021-01-07 18:50:02');
INSERT INTO `cnpscy_versions` VALUES (5, '1.0.4', '1.0.4', '[1.0.4 分支](https://gitee.com/clown-passerby-community/laravel-vue-admin/tree/1.0.4)\n\n* **同步使用多语言包;**\n* **update readme。**', 1610095993, 1610096019, 0, 5, '2021-01-07 23:00:10');

SET FOREIGN_KEY_CHECKS = 1;
