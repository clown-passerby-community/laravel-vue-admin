import request from '@/utils/request'

export function getList(params) {
    return request({
        url: 'adminloginlogs',
        method: 'get',
        params
    })
}

export function setDel(data, get_url = false) {
    var url = `/adminloginlogs/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}
