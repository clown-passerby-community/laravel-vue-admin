import request from '@/utils/request'

export function getRolesSelect() {
    return request({
        url: '/admin_roles/getSelectLists',
        method: 'get'
    })
}

export function getList(params, get_url = false) {
    var url = '/admin_roles';
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    })
}

export function create(data, get_url = false) {
    var url = `/admin_roles/create`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function update(data, get_url = false) {
    var url = `/admin_roles/update`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}

export function setDel(data, get_url = false) {
    var url = `/admin_roles/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}

export function changeFiledStatus(data, get_url = false) {
    var url = `/admin_roles/changeFiledStatus`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    })
}
