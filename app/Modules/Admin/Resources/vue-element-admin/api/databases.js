import request from '@/utils/request'

export function getList(params, get_url = false) {
    var url = `/database/tables`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    });
}

export function backupsTables(data, get_url = false) {
    var url = `/database/backupsTables`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'post',
        data
    });
}

// 备份记录列表
export function getBackupsList(params, get_url = false) {
    var url = `/database/backups`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    });
}

export function deleteBackup(data, get_url = false) {
    var url = `/database/deleteBackup`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}
