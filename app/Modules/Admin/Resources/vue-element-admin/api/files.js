import request from '@/utils/request'

// 文件列表
export function getFileList(params, get_url = false) {
    var url = `/getFileList`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
        params
    })
}

export function delFile(data, get_url = false) {
    var url = `/files/delete`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'delete',
        data
    })
}

export function removeFileGroup(data, get_url = false) {
    var url = `/files/removeFileGroup`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data
    });
}
