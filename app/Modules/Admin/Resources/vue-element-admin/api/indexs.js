import request from '@/utils/request'

// 首页 - 基础统计数据
export function statistics(get_url = false) {
    var url = `/indexs`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get'
    })
}

// 首页 - 请求日志统计表数据
export function logsStatistics(get_url = false) {
    var url = `/logsStatistics`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get'
    })
}

// 编辑登录管理员信息
export function updateAdmin(data, get_url = false) {
    var url = `/updateAdmin`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'put',
        data,
    })
}

// 版本历史记录
export function versionLogs(get_url = false) {
    var url = `/versionLogs`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
    })
}

// 服务器状态
export function getServerStatus(get_url = false) {
    var url = `/getServerStatus`;
    if (get_url) return url;
    return request({
        url: url,
        method: 'get',
    })
}
