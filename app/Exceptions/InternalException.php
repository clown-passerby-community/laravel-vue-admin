<?php

namespace App\Exceptions;

use Illuminate\Http\Request;

class InternalException extends Exception
{
    public function __construct(string $message = '系统内部错误', int $http_status = 500)
    {
        parent::__construct($message, $http_status);
    }

    public function render(Request $request)
    {
        if ($request->expectsJson()) {
            return response()->json(['msg' => $this->getMessage()], $this->getCode());
        }
    }
}
